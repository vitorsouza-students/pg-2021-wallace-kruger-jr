% ==============================================================================
% PG - Wallace Kruger Junior
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Fundamentação Teórica}
\label{sec-fundteo}

Este capítulo apresenta os principais conceitos teóricos que fundamentam o desenvolvimento da ferramenta de Gestão de Projetos e está organizado em 6 seções. A Seção~\ref{sec-fundteo-engsoft} apresenta os conceitos básicos e fundamentais da Engenharia de Software. A Seção~\ref{sec-fundteo-devweb} descreve sucintamente o desenvolvimento Web. A Seção~\ref{sec-fundteo-arq} apresenta as arquiteturas de softwares adotadas para o desenvolvimento da ferramente proposta. A Seção~\ref{sec-fundteo-frameweb} apresenta o método FrameWeb. A Seção~\ref{sec-fundteo-gestproj} introduz conceitos e a evolução da Gestão de Projetos. Por fim, a Seção~\ref{sec-fundteo-metagel} aborda rapidamente definições das Metodologias Ágeis, em especial, o Scrum e o Kanban. 

%%% Início de seção. %%%
\section{Engenharia de Software}
\label{sec-fundteo-engsoft}

Para um usuário, cliente ou novato da área da Computação, desenvolver software é, muitas vezes, confundido com programação. Essa visão, para situações de baixa complexidade, pode ser suficiente. Porém, chega-se a um ponto em que, dado o tamanho ou a complexidade do problema que se pretende resolver, essa abordagem não é mais indicada. Analogamente, em outras áreas de conhecimento, como na Engenharia Civil, um bom pedreiro poderá ser capaz de construir uma casinha de cachorro sozinho, sem projetos com plantas baixas ou terraplanagens. Talvez não seja a melhor solução, mas o resultado poderá corresponder a expectativa inicial. Entretanto, para se construir uma ponte ou mesmo um edifício, faz-se necessário um estudo aprofundado, incluindo análises do solo, cálculos estruturais, isto é, uma abordagem de engenharia.   Num cenário semelhante surge a Engenharia de Software, visando melhorar a qualidade dos produtos de software e maximizar a produtividade no processo de desenvolvimento~\cite{falbo2014notasaula}.

A Engenharia de Software engloba processos, métodos e ferramentas que possibilitam a construção de sistemas complexos baseados em computador dentro do prazo e com qualidade~\cite{pressman2011engsoft}.

Para melhorar a qualidade do software, pilar da Engenharia de Software, é preciso notar que qualidade é um conceito de múltiplas facetas. Para um usuário do sistema, um bom software é o que atende suas necessidades, sendo fácil de usar, eficiente e confiável. Por uma outra perspectiva, para um desenvolvedor, um produto de boa qualidade é o de fácil manutenção. Já para um cliente, o produto deve agregar valor ao seu negócio. O que há de comum nas várias perspectivas apresentadas é que todas são focadas no produto final de software. Entretanto para que tais características sejam atingidas é necessário que a qualidade seja incorporada ao longo do processo de desenvolvimento do produto~\cite{falbo2014notasaula}.

Nas subseções a seguir, procurou-se apresentar uma visão geral e sucinta dos principais processos da Engenharia de Software.

%%% Início de subseção. %%%
\subsection{Especificação de Requisitos}
\label{sec-fundteo-engsoft-espcreq}

Para começarmos as discussões dessas etapas, inicialmente, podemos definir requisito de software, segundo a norma IEEE-90, como sendo:

\begin{enumerate}
    \item Uma capacidade que um usuário necessita para resolver um problema ou atingir um objetivo;

    \item Uma capacidade que deve ser atendida ou possuída por um sistema ou componente de um sistema para satisfazer um contrato, padrão, especificação ou outro documento formalmente imposto;

    \item O conjunto de todos os requisitos que formam a base para o desenvolvimento subseqüente de um software ou componentes de um software;
\end{enumerate}

Em outras palavras, os requisitos são as descrições do que o sistema deve fazer, os serviços e processos que atendam as necessidades do cliente ou restrições operacionais da organização. O processo de descobrir, analisar, documentar e verificar esses serviços e restrições é chamado de Engenharia de Requisitos~\cite{sommer2011engsoft}.

De maneira geral, o processo de Engenharia de Requisitos começa pelo levantamento de requisitos. Nesta fase, os usuários e clientes trabalham junto com o engenheiro de requisitos para entender as necessidades que o software deve atender e os problemas e deficiências dos sistemas atuais~\cite{falbo2014notasaula}.

Uma vez identificados requisitos, é possível iniciar atividades de análise, que visam o acordo na negociação entre usuários para resolver conflitos detectados, resultando em requisitos consistentes e sem ambiguidades. A etapa de análise é essencialmente uma atividade de modelagem, preocupando-se apenas com o domínio do problema e não com soluções técnicas. Diferentes modelos podem ser construídos para representar diferentes perspectivas~\cite{falbo2014notasaula}.

Tanto no levantamento de requisitos quanto na análise, é importante documentar requisitos e modelos. Para estas etapas utiliza-se o documento de especificação de requisitos: contendo uma lista dos requisitos de usuário levantado, junto a uma visão geral do problema a ser resolvido, além do registro de vários diagramas resultantes do trabalho de análise, como o modelos de casos de uso e o modelo estrutural. O documento produzido é, então, verificado e validado. Caso clientes, usuários e desenvolvedores estejam de acordo com os requisitos, o processo de desenvolvimento pode avançar; caso contrário, deve-se retornar à atividade correspondente para resolver os problemas identificados~\cite{falbo2014notasaula}.


%%% Início de subseção. %%%
\subsection{Projeto}
\label{sec-fundteo-engsoft-proj}

O objetivo da fase de projeto é produzir uma solução para o problema identificado modelado durante o levantamento e análise de requisitos, incorporando a tecnologia aos requisitos e projetando o que será construído na implementação~\cite{falbo2014notasaula}.

O projeto de software encontra-se no núcleo técnico do processo de desenvolvimento de software e corresponde à primeira atividade que leva em conta aspectos tecnológicos~\cite{pressman2011engsoft}. Independentemente do paradigma adotado, a fase de projeto deve produzir: um projeto de arquitetura do software, que visa definir os grandes componentes estruturais do software e seus relacionamentos, projeto de dados, que tem por objetivo projetar a estrutura de armazenamento de dados necessária para implementar o software, projeto de interfaces, que descreve como o software deverá se comunicar dentro dele mesmo, com outros sistemas e com pessoas que o utilizam e o projeto detalhado, que tem por objetivo refinar e detalhar a descrição dos componentes estruturais do software~\cite{falbo2014notasaula}.

%%% Início de subseção. %%%
\subsection{Implementação e Teste}
\label{sec-fundteo-engsoft-impltest}

Uma vez projetado o sistema, o estágio mais crítico desse processo é, naturalmente,  a implementação do sistema. A implementação pode envolver o desenvolvimento de programas em alto ou baixo nível de linguagens de programação, bem como customização e adaptação de sistemas de prateleira, para atender aos requisitos específicos de uma organização~\cite{sommer2011engsoft}.

É muito importante que haja padrões organizacionais para a fase de implementação. Esses padrões devem ser seguidos por todos os programadores e devem estabelecer, dentre outros, padrões de nomes de variáveis, formato de cabeçalhos de programas e formato de comentários, recuos e espaçamento, de modo que o código e a documentação a ele associada sejam claros para quaisquer membros da organização~\cite{falbo2014notasaula}.

Alguns aspectos de implementação particularmente importantes para a Engenharia de Software são: o reúso, os softwares são construídos por meio de reuso de componentes existentes ou sistemas, o gerenciamento de configuração, gerenciamento do versionamento de componentes de software e desenvolvimento \textit{host-target}, separação de ambiente de desenvolvimento e produção~\cite{sommer2011engsoft}.

Teste de software é o processo de executar um programa com o objetivo de encontrar defeitos~\cite{Myers2004}. Teste é uma atividade de verificação e validação do software e consiste na análise dinâmica do mesmo, isto é, na execução do produto de software com o objetivo de verificar a presença de defeitos no produto e aumentar a confiança de que o mesmo está correto~\cite{Rocha2001}.

Dada sua importância, testes não devem ser tratados apenas como uma atividade no ciclo de vida do software, mas sim como um processo. O processo de teste deve ocorrer em paralelo com outras atividades do processo de desenvolvimento de software (análise de requisitos, projeto de software e implementação) e envolve também atividades de planejamento~\cite{falbo2014notasaula}.

Os testes devem ser feitos em diversos níveis, começando pelos teste unitários, em que as unidades individuais de programa ou classes de objetos são testadas individualmente, testes de integração, em que várias unidades individuais são integradas para criar componente compostos e teste de sistema, em que alguns ou todos os componentes de um sistema estão integrados e o sistema é testado com um todo~\cite{sommer2011engsoft}.

%%% Início de subseção. %%%
\subsection{Entrega e Manutenção}
\label{sec-fundteo-engsoft-entrmanu}

Uma vez concluídas as etapas anteriores e o sistema aceito e instalado, se está chegando ao fim do processo de desenvolvimento de software. A entrega não é meramente uma formalidade. No momento em que o sistema (ou uma versão dele) é instalado no local de operação e devidamente aceito, é necessário, ainda, ajudar os usuários a entenderem e a se sentirem mais familiarizados com o sistema. Neste momento, duas questões são cruciais para uma transferência bem-sucedida: o treinamento,  para que os usuários e operadores possam operar o sistema adequadamente e a documentação,  como material de referência para a solução de problemas ou como informações adicionais~\cite{falbo2014notasaula}.

O desenvolvimento de um sistema termina quando o produto é entregue para o cliente e entra em operação. A partir daí, deve-se garantir que o sistema continuará a ser útil e atendendo às necessidades do usuário, o que pode demandar alterações no mesmo. Começa, então, a fase de manutenção ou evolução~\cite{Sanches2001}. 

Há muitas causas para a manutenção, dentre elas~\cite{Sanches2001}: falhas no processamento devido a erros no software e etc., levando à necessidade de modificações em funções existentes ou de inclusão de novas capacidades no sistema. O processo de manutenção é semelhante, mas não igual ao processo de desenvolvimento, e pode envolver atividades de levantamento de requisitos, análise, projeto, implementação e testes, agora no contexto de um software existente. Essa semelhança pode ser maior ou menor, dependendo do tipo de manutenção a ser realizada~\cite{falbo2014notasaula}.

%%% Início de seção. %%%
\section{Desenvolvimento Web}
\label{sec-fundteo-devweb}

A Internet nasceu de um projeto de pesquisa militar (ARPA: \textit{Advanced Research Projects Agency}), no período da guerra fria, no final dos anos cinquenta e início dos anos sessenta. Este projeto surgiu como resposta do governo americano ao lançamento do Sputnik, pela ex-União Soviética. Através da rede, o governo norte-americano se protegeria e garantiria a fluência das comunicações, caso a guerra fria e os momentos posteriores ao evento histórico fossem favoráveis à ascensão da União Soviética. Mas a linguagem utilizada nos computadores ligados em rede era muito complicada, por isso, na época, o potencial de alastramento da Internet não podia ser imaginado~\cite{MerkleRichardson2000}.

A \textit{World Wide Web}, mais lembrada nos dias atuais como, simplesmente, Web, começou em 1989 no \textit{European Center for Nuclear Research} (CERN), apresentando uma estrutura arquitetônica que permite o acesso a documentos vinculados espalhados por milhões de máquinas na Internet. Desde então, a Web cresceu de forma desproporcional e descontrolada. Em 1994, por exemplo, havia menos de 3.000 sites online. Em 2014, havia mais de 1 bilhão. Isso representa um aumento de 33 milhões por cento em 20 anos~\cite{lafrance2015many}.

A Web pode ser analisada do ponto de vista da arquitetura ou do cliente, conforme almejou-se apresentar sumariamente nas próximas duas subseções a seguir.

%%% Início de subseção. %%%
\subsection{Arquitetura}
\label{sec-fundteo-devweb-arq}

A Web pode ser vista como uma vasta coleção mundial de documentos, comumente chamados de páginas. As páginas, majoritariamente, são visualizadas com o auxílio de um software chamado navegador, pode-se destacar os navegadores mais usados em 2021, segundo A 4GNEWS, como sendo o Google Chrome, Safari e Mozilla Firefox.

O navegador exibe uma página Web na máquina do cliente quando envia uma solicitação a um ou mais servidores, que respondem com o conteúdo da página. O protocolo de solicitação-resposta para buscar páginas é simples, baseado em texto, que roda sobre o \textit{Transmission Control Protocol} (TCP). Ele é chamado \textit{Hyper Text Transfer Protocol} (HTTP). A especificação, elaborada pelo W3C, o define como:

``HTTP é um protocolo de nível de aplicação para sistemas de hipermídia, colaborativos e distribuídos. É um protocolo genérico, sem estado e orientado a objetos que pode ser usado para diversas tarefas, tais como servidores de nomes e sistemas de gerenciamento de objetos distribuídos, através da extensão de seus métodos de requisição''~\cite{fielding1997}.

O protocolo HTTP é o ingrediente mais básico sobre o qual a Web está fundamentada. Através dele, o cliente (navegador ou dispositivo que fará a requisição) pode requisitar recursos disponíveis a um servidor remoto. O conteúdo pode simplesmente ser um documento que é lido de um disco, ou o resultado de uma consulta de banco de dados. Quanto à página, ela pode ser uma página estática, se apresentar o mesmo documento toda vez que for exibida, ou uma página dinâmica, se ela foi gerada sob demanda por um programa ou se contém um programa. Uma página dinâmica pode se apresentar de forma diferente toda vez que for exibida~\cite{Tanenbaum2011}. Como esses recursos estão distribuídos ao longo de toda a Internet, é necessário um mecanismo de identificação que permita localizá-los e acessá-los. O identificador usado para referenciar esses recursos é uma URL~\cite{Casteleyn2009}.

%%% Início de subseção. %%%
\subsection{Cliente}
\label{sec-fundteo-devweb-cli}

Cada página recebe um \textit{Uniform Resource Locator} (URL), que efetivamente serve como o nome identificador da página. As URLs têm pelo menos três partes: o protocolo, também chamado de esquema, usado para transferência de arquivos, o \textit{host} ou nome \textit{Domain Name System} (DNS) da máquina em que a página está localizada e o caminho que identifica exclusivamente a página específica. No caso geral, o caminho tem um nome hierárquico que modela uma estrutura de diretório de arquivo. Porém, a interpretação do caminho fica a critério do servidor; ele pode ou não refletir a estrutura de diretório real.

O \textit{HyperText Markup Language} (HTML), é uma linguagem de marcação utilizada na construção de páginas na Web, marcando os arquivos de texto lidos pelos navegadores e enviados pelos servidores. Não se trata de uma linguagem de programação. Um arquivo HTML é um arquivo de texto comum recheado de marcadores que se destacam do texto pelos caracteres especiais ``<'' e ``>''. Existem dois tipos de extensões do lado do cliente, os componentes que  funcionam como extensões executadas como se fossem parte do navegador e os scripts que estendem a linguagem HTML. Os scripts são interpretados enquanto o navegador carrega a página. O próprio código HTML é um script que é interpretado pelo navegador para definir a estrutura da página~\cite{rocha1999}. Um grande exemplo de script em alto nível, é a linguagem de programação interpretada JavaScript. Atualmente é a principal linguagem de programação Web no lado do cliente. Entretanto, também é muito utilizada no lado do servidor por meio de ferramentas como, por exemplo, o Node.js. 

Nas próximas subseções, serão apresentados as plataformas Web baseadas no JavaScript, de forma breve, que serão utilizadas no desenvolvimento da ferramenta de gestão de projetos.

%%% Início de subseção. %%%
\subsection{Angular}
\label{sec-fundteo-devweb-ang}

O Angular é um \textit{framework} Javascript, criado pelos desenvolvedores do Google, não apenas para construção de interface de usuário, mas também para aplicações únicas do lado do cliente, sejam elas para a Web, \textit{mobile} ou \textit{desktop}.

O Angular possui alguns elementos básicos que tornam essa construção mais interessante, podemos destacar os componentes, serviços, módulos, diretivas e ferramentas de infraestrutura que automatizam tarefas, como a execução de testes unitários. Conta com uma ferramenta chamada de Angular CLI, ferramenta de linha de comando que facilita a criação desses elementos.

O \textit{framework} é ideal para construção de \textit{single-page applications} (SPAs), aplicações nas quais o usuário não precisa aguardar o recarregamento de toda a página esperando uma atualização completa ou parcial, melhorando principalmente a usabilidade da aplicação.

%%% Início de subseção. %%%
\subsection{Node.js}
\label{sec-fundteo-devweb-node}

Node.js é uma tecnologia de execução assíncrona orientada a eventos, usada para executar código JavaScript fora do navegador. Com ele podemos construir aplicações Web em geral, desde \textit{websites} até APIs e microsserviços. Isso é possível pela união do ambiente de execução de JavaScript fornecido pelo próprio Node.js e o motor de interpretação e execução de JavaScript presente no Google Chrome, chamado de V8, que foi criado com o objetivo de permitir redes escaláveis. Node.js usa um modelo de E/S (Entrada e Saída) orientado a eventos e não bloqueante, o que o torna leve e eficiente, ideal para aplicações em tempo real com troca intensa de dados executadas em dispositivos distribuídos~\cite{node-js}.

Uma das principais características de um servidor feito com Node.js é o fato de sua execução ser \textit{single-threaded}: a cada conexão, o retorno de chamada é disparado, mas se não houver trabalho a ser feito, o Node.js ficará inativo. Isso contrasta com outros servidores tradicionais que possuem uma execução \textit{multi-threaded}, no qual \textit{threads} do sistema operacional são empregadas. A rede baseada em \textit{thread} é relativamente ineficiente e muito difícil de usar. Além disso, os usuários do Node.js estão livres da preocupação de travar o processo. Quase nenhuma função no Node.js realiza E/S diretamente, portanto, o processo nunca é bloqueado. Como nada bloqueia, sistemas escaláveis são muito razoáveis para desenvolver em Node.js~\cite{node-js}.

Porém, o Node.js, não possui suporte nativo para tarefas comuns no desenvolvimento Web, como o tratamento para métodos HTTP, ou especificações de rotas. Ainda assim, resolve-se facilmente utilizando um \textit{framework}. Na ferramenta de gestão de projetos, foco desse texto, será utilizado o \textit{framework} mais popular de Node.js, o Express.js. Este \textit{framework}, fornece um conjunto robusto de recursos, completando o arsenal Node e possibilitando o desenvolvimento de um sistema Web escalável.

%%% Início de seção. %%%
\section{Arquitetura de Software}
\label{sec-fundteo-arq}

A seguir, serão apresentadas as arquiteturas de software que foram escolhidas para o desenvolvimento do sistema GanttBox. Em cada subseção, tentou-se apresentar de forma sucinta e instrutiva as arquiteturas, de forma que também expusessem sua vantagens e os motivos da escolha. 

\subsection{Clean Architecture}
\label{sec-fundteo-arq-clean}
A arquitetura de software do sistema de gestão de projetos baseia-se primordialmente na \textit{Clean Architecture}, ou Arquitetura Limpa, proposta idealmente por Robert Cecil Martin~\cite{martin2018clean}. A Figura~\ref{figura-arquitetura} mostra a arquitetura do sistema.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/fig-arquitetura-limpa.jpg}
	\caption{Arquitetura proposta pela \textit{Clean Architecture}~\cite{martin2018clean}.}
	\label{figura-arquitetura}
\end{figure}

O núcleo, camada mais interna do diagrama, é a camada de Entidades (\textit{Entities}), as classes modelos, que representam as tabelas ou \textit{collections} do banco de dados dentro do projeto. Numa camada acima, de comunicação direta com a entidades, observamos os Casos de Uso (\textit{Use Cases}), onde se encontram todas as regras de negócio da aplicação. Em seguida, a camada dos Adaptadores (\textit{Adapters}), esta camada é a camada dos controladores, que se comunicam com as requisições advindas dos elementos externos. Concluímos com a camada mais externa, \textit{frameworks} externos, onde se encontram todos os elementos externos que necessitam se comunicar com a nossa camada de entidades, como bancos de dados, \textit{frameworks} e interfaces.

São vários os princípios das boas práticas de programação seguidos na obra para adoção da arquitetura limpa que, quando bem aplicada, permite, entre vários benefícios, escalar o projeto de software com facilidade. Um dos principais padrões que podemos pontuar é o SOLID, segundo \citeonline{martin2018clean}, um acrônimo para os seguintes princípios:

\begin{itemize}
	    \item Princípio da responsabilidade única (\textit{Single-responsibility principle}): uma classe deve ter um, e somente um, motivo para mudar;
	
	    \item Princípio aberto-fechado (\textit{Open–closed principle}): objetos ou entidades devem estar abertos para extensão, mas fechados para modificação;
	
	    \item Princípio de substituição de Liskov (\textit{Liskov substitution principle}): uma classe derivada deve ser substituível por sua classe base;
	    
	    \item Princípio de segregação de interfaces (\textit{Interface segregation principle}): uma classe não deve ser forçada a implementar interfaces e métodos que não irão utilizar;
	    
	    \item Princípio da inversão de dependência (\textit{Dependency inversion principle}): dependa de abstrações e não de implementações.
\end{itemize}

Existem inúmeras vantagens em se utilizar uma arquitetura dividida em camadas, em especial, projetos de software que adotam a arquitetura limpa com maestria apresentam as seguintes características:

\begin{itemize}
	    \item Independência de qualquer agente externo:  a arquitetura não depende de nenhum \textit{framework} ou biblioteca, permitindo o uso das ferramentas como apoio ao desenvolvimento ao invés de limitar o projeto com seus respectivos padrões;
	
	    \item Independência de banco de dados: as regras de negócio não estão ligadas as particularidades de um determinado banco, a troca de um banco Oracle para PostgreSQL ou MongoDB, pode ser feita sem impacto no funcionamento do negócio;
	
	    \item Independência da interface com o usuário: a interface do usuário pode ser facilmente substituída sem impactar o restante do sistema, uma interface console pode ser trocada por uma Web, sem o impactar as regras de negócio;
	    
	    \item Testabilidade: como as camadas estão bem definidas, as regras de negócio podem ser testadas sem a interface do usuário, banco de dados, servidores Web ou qualquer outro elemento externo.
\end{itemize}

Numa arquitetura bem definida, as dependências apontam para o nível mais alto da aplicação, analogamente, na \textit{clean architecture}, as dependências apontam para as camadas mais internas. As entidades não devem saber nada sobre a camada de Casos de Uso, assim como a camada de Casos de Uso não deve saber nada sobre a camada mais externa e assim sucessivamente. Na prática, a alteração de um controlador não deve interferir em nada no comportamento dos casos de usos, igualmente nada nas entidades, as camadas mais externas não influenciam no comportamento das camadas mais internas. Uma arquitetura separada em camadas poupará muito esforço dos desenvolvedores com futuro problemas de manutenção do código. Um problema de regra de negócio específico estará na classe especialista daquele assunto. A regra de dependência torna o sistema completamente testável. Assim, quando uma biblioteca ou banco de dados se torna obsoleto, a troca do mesmo poderá ser feita de forma hábil, garantindo integridade do sistema como um todo.

\subsection{Multitenancy}
\label{sec-fundteo-arq-tenant}

Softwares empresariais tradicionais são instalados em computadores e precisam de um administrador para mantê-los. Esta maneira tradicional de operação pode limitar um negócio quando diferentes departamentos precisam trabalhar a partir de uma mesma base da dados, ou simplesmente pelo custo de infraestrutura. Com  a computação em nuvem, atualmente, um modelo de disponibilizar serviços de software vem ganhando espaço no mercado, os chamados Saas (\textit{Software as a Service}), onde uma empresa disponibiliza uma solução tecnológica pela Internet. Com este modelo, a empresa que contrata o serviço desta empresa fornecedora da solução só precisará de acesso à Internet para utilizar os recursos desejados, isto é, uma empresa utilizando serviços de outra empresa, numa relação B2B (\textit{Business to Business}).

Nesse contexto, faz-se o projeto do sistema GanttBox, um sistema que será desenvolvido oferendo serviços de gestão, inicialmente implantado dentro de um cenário conhecido, mas intencionalmente pensado para escalar vários inquilinos. Diante disso, optou-se pela escolha da adoção da arquitetura \textit{multitenant}. \textit{Multitenant} ou \textit{multitenancy} é uma arquitetura de software projetada para que uma única instância da aplicação centralizada, seja executada em um servidor e que atenda vários clientes~\cite{krebs2012architectural}. 

Nos softwares tradicionais ou no modelo tradicional de arquitetura \textit{singletenant}, para cada novo cliente haveria uma instância da aplicação distinta servindo cada cliente de maneira isolada, nesse cenário, quando o software precisa-se ser atualizado, seria necessário também atualizar os inúmeros servidores, havendo risco de um ou mais clientes ficarem sem a devida atualização. O fato é, aplicando a arquitetura \textit{multitenant}, conseguimos diminuir a complexidade de gestão do software, pois tendo uma única instância de software não haverá risco de um pontual cliente ficar desatualizado. Além disso, podemos levantar a economia com infraestrutura, mesmo precisando de uma instância mais robusta para atender \textit{multitenants} será menos custosa de manter pensando em um número razoável de clientes.

Assumindo esta arquitetura como mais adequada para este sistema, caímos na questão da segregação dos dados: como existirá uma única instância de software, precisamos separar os dados dos diferentes \textit{tenants} da nossa aplicação. A abordagem que optou-se para o desenvolvimento do sistema de gestão foi a segregação dos dados via esquemas diferentes. Nesta estratégia, existe uma única instância de banco de dados, mas essa instância possui múltiplos conjuntos de tabelas separados por cada \textit{tenant}. Na aplicação que está sendo proposta nesse texto, toda vez que um cliente criar uma conta como representação de uma instituição, será criado um novo esquema de banco dados exatamente com as mesma modelagem de tabelas. Esta estratégia é mais barata, pois se usará uma única instância de banco e ainda garantirá o isolamento lógico dos dados de cada cliente por esquemas.

%%% Início de seção. %%%
\section{FrameWeb}
\label{sec-fundteo-frameweb}

FrameWeb surgiu como um método de projeto para construção de sistemas de informação Web (\textit{Web Information Systems} -- WISs) baseado em \textit{frameworks}. Em linhas gerais, assume-se que determinados tipos de \textit{frameworks} serão utilizados durante a construção da aplicação, define uma arquitetura básica para o WIS e propõe modelos de projeto que se aproximam da implementação do sistema usando esses \textit{frameworks}~\cite{souza:masterthesis07}.

Tanto o método FrameWeb quanto sua ferramentas, estão em constante desenvolvimento, pesquisadores seguem trabalhando em diferentes aspectos da proposta. Atualmente, o método dedica suas atenções estritamente na fase de projeto arquitetural, sendo elas:

\begin{itemize}
    \item Definição de uma uma arquitetura lógica padrão para WISs, que divide o sistema em camadas, baseada no padrão arquitetônico \textit{Service Layer} (Camada de Serviço), proposto por Randy Stafford em~\cite{Fowler2002}. Segundo \citeonline{souza:celebratingfalbo20}, a arquitetura é composta por três camadas conforme a Figura~\ref{fig-fundteo-engsoft-frameweb}:

    \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{figuras/arq-frameweb.JPG}
        \caption{Arquitetura Proposta pelo FrameWeb~\cite{campos-souza:webmedia17}.}
        \label{fig-fundteo-engsoft-frameweb}
    \end{figure}

	\begin{itemize}
	    \item O pacote Visão contém arquivos relacionados exclusivamente com a exibição de informações ao usuário, como páginas Web, folhas de estilo, imagens e scripts que executam do lado do cliente. O pacote Controle, envolve classes que controlam as requisições vindas do usuário. Juntos formam a primeira camada, \textbf{Apresentação}, que tem por objetivo prover interfaces gráficas com o usuário;
	
	    \item O pacote de Domínio contém classes que representam conceitos do domínio do problema identificados e modelado pelos diagramas de classes na fase de requisitos e refinados durante o projeto. O pacote Aplicação, tem a responsabilidade de implementar os casos de uso definidos na especificação de requisitos, provendo uma camada de serviços independente da interface com o usuário. Juntos, esses pacotes consolidam a lógica de negócio e formam a segunda camada, \textbf{Negócio};
	
	    \item A terceira e última camada, \textbf{Acesso de Dados}, contém apenas o pacote Persistência, que é responsável pelo armazenamento dos objetos persistentes em mídia de longa duração, como banco de dados, arquivos, serviços de nome, etc.
	\end{itemize}

    \item A partir da arquitetura definida, é proposto um conjunto de modelos de projeto que trazem conceitos utilizados pelos \textit{frameworks} para esta fase do processo mediante a criação de um perfil, baseado no diagrama de classes da UML, que aproxima a fase de implementação, engenhando o desevolvimento com base nesses modelos. Esses modelos, segudo~\cite{campos-souza:webmedia17}, se dividem em quatro tipos:

   	\begin{itemize}
	   	\item \textbf{Modelo de Entidades}: representa os objetos de domínio do problema e seu mapeamento para a persistência em banco de dados;
	
	    \item \textbf{Modelo de Aplicação}: representa as classes de serviço, responsáveis pela implementação das funcionalidades do WIS, e suas dependências;
	    	    
	    \item \textbf{Modelo de Persistência}: representa as classes Data Access Object (DAO) existentes, responsáveis pela persistência das instâncias das classes de domínio;
	
	    \item \textbf{Modelo de Navegação}: representa os diferentes componentes que formam a camada de apresentação, como páginas Web, formulários HTML, etc.
	\end{itemize}
\end{itemize}

%%% Início de seção. %%%
\section{Gestão de Projetos}
\label{sec-fundteo-gestproj}

Projeto é um esforço temporário empreendido para criar um produto, serviço ou resultado único~\cite{Cruz2013}. Assim como a Engenharia de Software nasceu com o intuito de prestar um tratamento mais sistemático no processo de desenvolvimento de software, numa linha muito semelhante, a gestão de projetos se apresenta com a estruturação da forma como o projeto é planejado, executado, monitorado e controlado, independentemente da área de aplicação, a fim de otimizá-lo e proporcionar uma série de benefícios.

A gestão de projetos atual se desenvolveu para incluir indústrias, como setores de produção de energia, siderúrgicas e quaisquer outros ramos, possuindo uma tendência ágil, com características de equipes auto gerenciadas e autônomas para tomar decisões~\cite{conforto2014can}. Entretanto, a gestão de projetos existe há milhares de anos. Construções como a pirâmide de Gizé e a muralha da China indagam arquitetos e engenheiros ainda hoje sobre como conseguiram tal feito. Ao longo da história, vários foram os fatores que contribuíram para evoluir e moldar a gestão de projetos como articulada nos dias atuais. 

Devemos lembrar de Henry Gantt, considerado por muitos o fundador da gestão de projetos. Na primeira metade do século 20, os líderes empresariais começaram a enfrentar os desafios das leis e regulamentos trabalhistas do governo. Gantt desenvolveu técnicas de planejamento e controle para ajudar as empresas a terem sucesso e cumprirem os regulamentos. 

Outro marco importante a se citar é a Associação Internacional de Gestão de Projetos (IPMA), iniciada em Viena no ano de 1965, visando promover a gestão de projetos e liderar o desenvolvimento da profissão.

Como já vem sendo discutido ao longo desse texto, o avanço da Computação está contribuindo de forma notável e imediata nas estratégias de negócios das organizações, moldando a gestão de projetos. Hoje, a automação dessa gestão em um empreendimento não é mais um luxo, e sim, uma necessidade para a empresa se manter competitiva no mercado.

A ideia que motiva esse trabalho não é revolucionária, nem mesmo novidade. O mercado de hoje está recheado de ferramentas que articulam a gerência de projetos, porém a grande maioria dessas ferramentas desempenha papéis pontuais no meio do processo de gestão ou são financeiramente inacessíveis para pequenas empresas. A ferramenta de gestão que está sendo pensada e proposta nesse trabalho mira neste alvo e tenta abranger a grande maioria de funcionalidades desses softwares que acredita-se ser suficientes para gerir um pequeno negócio.

%%% Início de seção. %%%
\section{Metodologias Ágeis}
\label{sec-fundteo-metagel}

Como vem sendo mencionado ao longo deste texto, à medida que se aumenta a complexidade de um produto, especificamente aqui, de um \textit{software}, se faz necessário seguir metodologias para estruturar e sistematizar toda a complexidade do negócio envolvida na solução.

Especialmente na década de 1990, a indústria de desenvolvimento de \textit{software} enfrentava dificuldades nos seus projetos por tentar imitar estratégias de gestão que funcionavam bem em outras áreas da Engenharia, porém não se enquadravam perfeitamente para projetos de \textit{softwares}. No decorrer na década, foram surgindo métodos que se mostravam mais assertivos. Assim, em 2001 representantes destes métodos se reuniram em Utah, Estados Unidos, percebendo que os métodos possuíam filosofias fundamentais em comum, desta forma, durante este encontro, foram definidos vários valores e princípios que hoje são conhecidos como ``O Manifesto Ágil''~\cite{fowler2001agile}. Métodos que seguem estas filosofias são então chamados de métodos ágeis.

Scrum e Kanban são dois métodos ágeis muitos utilizados atualmente. Na prática, estes métodos e customizações dos mesmos acabam sendo combinados para melhor atender as necessidades do negócio. O mesmo ocorre nas estratégias de otimizações gerenciais propostas na ferramenta produzida neste trabalho.

%\subsection{Scrum}
%\label{sec-fundteo-metagel-scrum}

O Scrum é um método ágil, que define um processo de desenvolvimento de projetos focado nas pessoas da equipe. O nome ``Scrum'' surgiu da rápida reunião que ocorre quando os jogadores do esporte \textit{rugby} vão iniciar um lance. A analogia entre jogadores e desenvolvedores foi usada pois cada time do esporte age em conjunto como uma unidade integrada, cada membro desempenha um papel específico e
todos se ajudam em busca de um objetivo comum. Ele baseia-se em algumas características~\cite{schwaber1997scrum}: flexibilidade dos resultados, flexibilidade dos prazos, times pequenos,
revisões frequentes e colaboração.

%\subsection{Kanban}
%\label{sec-fundteo-metagel-kanban}

O Kanban é um método ágil, que visa identificar possíveis gargalos, realizando as devidas correções para que haja fluidez nas atividades de uma empresa. O nome ``Kanban'' é de origem japonesa e significa ``sinalização'' ou ``cartão''. Sendo um método visual que busca gerenciar o trabalho conforme a movimentação de cartões, que nesta monografia chamamos de \textit{cards}. A proposta do uso de cartões somado as suas movimentações, visa acompanhar o andamento da produção de uma atividade dentro de um fluxo de trabalho de uma equipe~\cite{huang1996overview}.

