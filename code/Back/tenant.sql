-- -----------------------------------------------------
-- Table `document`
-- -----------------------------------------------------
CREATE TABLE document (
    id SERIAL NOT NULL,
    name VARCHAR(255) NOT NULL,
    name_s3 VARCHAR(255) NOT NULL,
    
    CONSTRAINT pk_document PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table `address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS address (
    id SERIAL NOT NULL,
	zip_code VARCHAR(10) NOT NULL,
	street VARCHAR(255) NOT NULL,
	neighborhood VARCHAR(255) NOT NULL,
	number_address INT NOT NULL,
	complement VARCHAR(255) NOT NULL,
	lat NUMERIC,
	lng NUMERIC,
	city_id INT4 NOT NULL,
	
	CONSTRAINT pk_address PRIMARY KEY (id),
	CONSTRAINT fk_address_city FOREIGN KEY (city_id)
	    REFERENCES public.city(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- -----------------------------------------------------
-- Table `contact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS contact (
    id SERIAL NOT NULL,
	email VARCHAR(255) NOT NULL,
	telephone VARCHAR(255),
	cellphone VARCHAR(255) NOT NULL,
	
	CONSTRAINT pk_contact PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table `person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS person (
    id SERIAL NOT NULL,
	name VARCHAR(255) NOT NULL,
	cpf VARCHAR(255) NOT NULL,
	address_id INT4,
	contact_id INT4 NOT NULL,
	
	CONSTRAINT pk_person PRIMARY KEY (id),
	CONSTRAINT fk_person_address FOREIGN KEY (address_id)
	    REFERENCES address(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_person_contact FOREIGN KEY (contact_id)
	    REFERENCES contact(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION

);

-- -----------------------------------------------------
-- Table `sector`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS sector (
    id SERIAL NOT NULL,
	code SERIAL NOT NULL,
	name VARCHAR(255) NOT NULL,
	
	CONSTRAINT pk_sector PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table `collaborator_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS collaborator_data (
    id SERIAL NOT NULL,
	rg VARCHAR(255) NOT NULL,
	cpf VARCHAR(255) NOT NULL,
	pis_pasep VARCHAR(255),
	schooling VARCHAR(255),
	number_of_dependents INT,
	civil_status VARCHAR(255),
	gender VARCHAR(255),
	
	CONSTRAINT pk_collaborator_data PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table `bank_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS bank_data (
    id SERIAL NOT NULL,
	bank VARCHAR(255) NOT NULL,
	agency VARCHAR(255) NOT NULL,
	account VARCHAR(255) NOT NULL,
	
	CONSTRAINT pk_bank_data PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table `collaborator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS collaborator (
    id SERIAL NOT NULL,
	code SERIAL NOT NULL,
	manager BOOLEAN NOT NULL,
	user_id INT4 NOT NULL,
	person_id INT4 NOT NULL,
	collaborator_data_id INT4 NOT NULL,
	bank_data_id INT4,
	sector_id INT4,
	
	CONSTRAINT pk_collaborator PRIMARY KEY (id),
	CONSTRAINT fk_collaborator_user FOREIGN KEY (user_id)
	    REFERENCES public.user(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_collaborator_person FOREIGN KEY (person_id)
	    REFERENCES person(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_collaborator_collaborator_data FOREIGN KEY (collaborator_data_id)
	    REFERENCES collaborator_data(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_collaborator_bank_data FOREIGN KEY (bank_data_id)
	    REFERENCES bank_data(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_collaborator_sector FOREIGN KEY (sector_id)
	    REFERENCES sector(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- -----------------------------------------------------
-- Table `client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS client (
    id SERIAL NOT NULL,
	code SERIAL NOT NULL,
	user_id INT4 NOT NULL,
	person_id INT4 NOT NULL,

	CONSTRAINT pk_client PRIMARY KEY (id),
	CONSTRAINT fk_client_user FOREIGN KEY (user_id)
	    REFERENCES public.user(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_client_person FOREIGN KEY (person_id)
	    REFERENCES person(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- -----------------------------------------------------
-- Table `subsystem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS subsystem (
    id SERIAL NOT NULL,
	name VARCHAR(255) NOT NULL,
	
	CONSTRAINT pk_subsystem PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table `funnel_slot`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS funnel_slot (
    id SERIAL NOT NULL,
	title VARCHAR(255) NOT NULL,
	color VARCHAR(255),
	order_funnel INT NOT NULL,
	subsystem_id INT4 NOT NULL,
	
	CONSTRAINT pk_funnel_slot PRIMARY KEY (id),
	CONSTRAINT fk_funnel_slot_subsystem FOREIGN KEY (subsystem_id)
	    REFERENCES subsystem(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- -----------------------------------------------------
-- Table `lead`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS lead (
    id SERIAL NOT NULL,
	gaint BOOLEAN,
	funnel_slot_id INT4 NOT NULL,
	person_id INT4 NOT NULL,
	client_id INT4,
	
	CONSTRAINT pk_lead PRIMARY KEY (id),
	CONSTRAINT fk_lead_funnel_slot FOREIGN KEY (funnel_slot_id)
	    REFERENCES funnel_slot(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_lead_person FOREIGN KEY (person_id)
	    REFERENCES person(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_lead_client FOREIGN KEY (client_id)
	    REFERENCES client(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- -----------------------------------------------------
-- Table `proposal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS proposal (
    id SERIAL NOT NULL,
	code SERIAL NOT NULL,
	description TEXT NOT NULL,
	send_date TIMESTAMP,
	value NUMERIC,
	lead_id INT4 NOT NULL,
	
	CONSTRAINT pk_proposal PRIMARY KEY (id),
	CONSTRAINT fk_proposal_lead FOREIGN KEY (lead_id)
	    REFERENCES lead(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- -----------------------------------------------------
-- Table `contract_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS contract_status (
    id SERIAL NOT NULL,
	name VARCHAR(255) NOT NULL,
	
	CONSTRAINT pk_contract_status PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table `contract`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS contract (
    id SERIAL NOT NULL,
	code SERIAL NOT NULL,
	initial_date TIMESTAMP,
	final_date TIMESTAMP,
	proposal_id INT4 NOT NULL,
	contract_status_id INT4 NOT NULL,
	client_id INT4,
	
	CONSTRAINT pk_contract PRIMARY KEY (id),
	CONSTRAINT fk_contract_proposal FOREIGN KEY (proposal_id)
	    REFERENCES proposal(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_contract_contract_status FOREIGN KEY (contract_status_id)
	    REFERENCES contract_status(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_contract_client FOREIGN KEY (client_id)
	    REFERENCES client(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- -----------------------------------------------------
-- Table `project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS project (
    id SERIAL NOT NULL,
	code SERIAL NOT NULL,
	name VARCHAR(255) NOT NULL,
	
	CONSTRAINT pk_project PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Inserindo dados em subsystem
-- -----------------------------------------------------
INSERT INTO subsystem (name) VALUES
('UserManagement'),
('SalesManagement'),
('ProjectManagement');

-- -----------------------------------------------------
-- Inserindo dados em funnel_slot
-- -----------------------------------------------------
INSERT INTO funnel_slot (title, order_funnel, subsystem_id) VALUES
('Prospecção', 1, (SELECT id FROM subsystem WHERE name='SalesManagement')),
('Interesse', 2, (SELECT id FROM subsystem WHERE name='SalesManagement')),
('Proposta', 3, (SELECT id FROM subsystem WHERE name='SalesManagement')),
('Contrato', 4, (SELECT id FROM subsystem WHERE name='SalesManagement')),
('Backlog', 1, (SELECT id FROM subsystem WHERE name='ProjectManagement')),
('Análise', 2, (SELECT id FROM subsystem WHERE name='ProjectManagement')),
('A Executar', 3, (SELECT id FROM subsystem WHERE name='ProjectManagement')),
('Desenvolvimento', 4, (SELECT id FROM subsystem WHERE name='ProjectManagement')),
('Teste Cruzado', 5, (SELECT id FROM subsystem WHERE name='ProjectManagement')),
('Homologação', 6, (SELECT id FROM subsystem WHERE name='ProjectManagement')),
('Produção', 7, (SELECT id FROM subsystem WHERE name='ProjectManagement'));

-- -----------------------------------------------------
-- Inserindo dados em contract_status
-- -----------------------------------------------------
INSERT INTO contract_status (name) VALUES
('Iniciado'),
('Selado'),
('Cancelado'),
('Vigente'),
('Concluído');



