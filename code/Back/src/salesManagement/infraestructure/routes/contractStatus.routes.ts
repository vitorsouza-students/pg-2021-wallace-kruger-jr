import { Router } from 'express'
import { ContractStatusController } from '../../interfaces/controllers/ContractStatusController';

export class ContractStatusRoute {

  public contractStatusRouter: Router;
  public contractStatusController: ContractStatusController;

  constructor() {
    this.contractStatusRouter = Router();
    this.contractStatusController = new ContractStatusController();
  }

  public routes() {
    this.contractStatusRouter.get('', this.contractStatusController.findAll);

    return this.contractStatusRouter;
  }
}

export default new ContractStatusRoute().routes();