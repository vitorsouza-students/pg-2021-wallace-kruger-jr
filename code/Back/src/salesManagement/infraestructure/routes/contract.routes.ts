import { Router } from 'express'
import { ContractController } from '../../interfaces/controllers/ContractController';

export class ContractRoute {

  public contractRouter: Router;
  public contractController: ContractController;

  constructor() {
    this.contractRouter = Router();
    this.contractController = new ContractController();
  }

  public routes() {
    this.contractRouter.get('', this.contractController.findAll);
    this.contractRouter.get('/:id', this.contractController.findByLead);
    this.contractRouter.post('', this.contractController.create);
    this.contractRouter.put('', this.contractController.update);
    this.contractRouter.delete('/:id', this.contractController.delete);

    return this.contractRouter;
  }
}

export default new ContractRoute().routes();