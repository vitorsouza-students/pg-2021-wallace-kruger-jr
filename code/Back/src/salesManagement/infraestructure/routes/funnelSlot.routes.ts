import { Router } from 'express'
import { FunnelSlotController } from '../../interfaces/controllers/FunnelSlotController';

export class FunnelSlotRoute {

  public funnelSlotRouter: Router;
  public funnelSlotController: FunnelSlotController;

  constructor() {
    this.funnelSlotRouter = Router();
    this.funnelSlotController = new FunnelSlotController();
  }

  public routes() {
    this.funnelSlotRouter.get('/:subsystem', this.funnelSlotController.find);

    return this.funnelSlotRouter;
  }
}

export default new FunnelSlotRoute().routes();