import { Router } from 'express'
import { CityController } from '../../interfaces/controllers/CityController';

export class CityRoute {

  public cityRouter: Router;
  public cityController: CityController;

  constructor() {
    this.cityRouter = Router();
    this.cityController = new CityController();
  }

  public routes() {
    this.cityRouter.get('/:uf', this.cityController.findCitysByUf);
    this.cityRouter.get('/uf/all', this.cityController.findUfs);
    return this.cityRouter;
  }
}

export default new CityRoute().routes();