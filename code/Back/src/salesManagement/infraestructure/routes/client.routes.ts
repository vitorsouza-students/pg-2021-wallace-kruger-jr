import { Router } from 'express'
import { ClientController } from '../../interfaces/controllers/ClientController';

export class ClientRoute {

  public clientRouter: Router;
  public clientController: ClientController;

  constructor() {
    this.clientRouter = Router();
    this.clientController = new ClientController();
  }

  public routes() {
    this.clientRouter.get('', this.clientController.findAll);
    this.clientRouter.post('', this.clientController.create);
    this.clientRouter.put('', this.clientController.update);
    this.clientRouter.delete('/:id', this.clientController.delete);

    return this.clientRouter;
  }
}

export default new ClientRoute().routes();