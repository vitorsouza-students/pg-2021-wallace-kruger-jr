import { Router } from 'express'
import { LeadController } from '../../interfaces/controllers/LeadController';

export class LeadRoute {

  public leadRouter: Router;
  public leadController: LeadController;

  constructor() {
    this.leadRouter = Router();
    this.leadController = new LeadController();
  }

  public routes() {
    this.leadRouter.get('', this.leadController.findAll);
    this.leadRouter.post('', this.leadController.create);
    this.leadRouter.put('', this.leadController.update);
    this.leadRouter.put('/changestatus', this.leadController.changeStatus);
    this.leadRouter.put('/declarewinloss', this.leadController.declareWinLoss);
    this.leadRouter.delete('/:id', this.leadController.delete);

    return this.leadRouter;
  }
}

export default new LeadRoute().routes();