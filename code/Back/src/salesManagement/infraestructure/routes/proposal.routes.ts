import { Router } from 'express'
import { ProposalController } from '../../interfaces/controllers/ProposalController';

export class ProposalRoute {

  public proposalRouter: Router;
  public proposalController: ProposalController;

  constructor() {
    this.proposalRouter = Router();
    this.proposalController = new ProposalController();
  }

  public routes() {
    this.proposalRouter.get('', this.proposalController.findAll);
    this.proposalRouter.get('/:id', this.proposalController.findByLead);
    this.proposalRouter.post('', this.proposalController.create);
    this.proposalRouter.put('', this.proposalController.update);
    this.proposalRouter.delete('/:id', this.proposalController.delete);

    return this.proposalRouter;
  }
}

export default new ProposalRoute().routes();