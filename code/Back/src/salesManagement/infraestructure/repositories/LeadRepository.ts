import { getConnection, getRepository, IsNull, Repository } from "typeorm";
import Lead from '../../domain/Lead';
import ILeadRepository from "./interfaces/ILeadRepository";


class LeadRepository implements ILeadRepository {

    private ormRepository: Repository<Lead>;
  
    constructor(private tenant: string) {
        this.ormRepository = getRepository(Lead);
    }

    public async findById(id: number): Promise<Lead | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const lead = this.ormRepository.findOne({ where: { id }});
        return lead;
    }
    
    public async findAllLeadsInTheFunnel(): Promise<Lead[] | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);
        
        const leads = this.ormRepository.find({ where: { gaint: IsNull() }});
        return leads;
    }

    public async create(lead: Lead): Promise<Lead | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const leadCreated = await this.ormRepository.create(lead);
        const leadSaved = await this.ormRepository.save(leadCreated);

        return leadSaved;
    }

    public async update(lead: Lead): Promise<Lead | undefined>{
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const leadUpdated = await this.ormRepository.save(lead);
        return leadUpdated;
    }

    public async delete(id: number): Promise<void> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        await this.ormRepository.delete(id);
    }

}

export default LeadRepository