import { getConnection, getRepository, IsNull, Repository } from "typeorm";
import AppError from "../../../shared/errors/AppError";
import Client from '../../domain/Client';
import IClientRepository from "./interfaces/IClientRepository";
import crypto from 'crypto';
import User from "../../../userManagement/domain/User";
import { hash } from 'bcryptjs'; 
import Contract from "../../domain/Contract";
import NodemailerProvider from "../../../shared/providers/Mail/NodemailerProvider";
import Lead from "../../domain/Lead";


class ClientRepository implements IClientRepository {

    private ormRepository: Repository<Client>;
  
    constructor(private tenant: string) {
        this.ormRepository = getRepository(Client);
    }

    public async findById(id: number): Promise<Client | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const client = this.ormRepository.findOne({ where: { id }});
        return client;
    }

    public async findByEmail(email: string): Promise<Client | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const client = await this.ormRepository.createQueryBuilder('client')
            .innerJoinAndSelect('client.person', 'person')
            .innerJoinAndSelect('person.contact', 'contact')
            .where('contact.email = :email', { email })
            .getOne();

        return client;
    }

    public async findByCpf(cpf: string): Promise<Client | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const client = await this.ormRepository.createQueryBuilder('client')
            .innerJoinAndSelect('client.person', 'person')
            .where('person.cpf = :cpf', { cpf })
            .getOne();

        return client;
    }
    
    public async findAllClientsPaged(page: number, limit: number): Promise<[Client[] | undefined, number]> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const offset = (page - 1) * limit;
        
        const [ clients, count] = await this.ormRepository.createQueryBuilder('client')
            .innerJoinAndSelect('client.person', 'person')
            .innerJoinAndSelect('person.contact', 'contact')
            .innerJoinAndSelect('person.address', 'address')
            .innerJoinAndSelect('address.city', 'city')
            .orderBy('person.name', 'ASC')
            .limit(limit)
            .offset(offset)
            .getManyAndCount();

        return [ clients, count];
    }

    public async create(client: Client): Promise<Client | undefined> {
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
    
        try {
            await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

            let newClient : Client = {
                code: client.code,
                userId: client.userId,
                user: client.user,
                personId: client.personId,
                person: client.person
            }
            
            const password = crypto.randomBytes(10).toString('hex');
            newClient.user.name = await this.verifyExistsUserWithSameName(client.person.name, client.person.contact.email);
            newClient.user.password = await hash(password, 8);
            newClient.user.isClient = true;
            newClient.user.isCollaborator = false;
            newClient.user.isManager = false;
            newClient.user.emailChecked = false;            

            const clientCreated = queryRunner.manager.create(Client, newClient);
            const clientSaved = await queryRunner.manager.save(clientCreated);

            let contract = await queryRunner.manager.findOne(Contract, client?.contracts[0]?.id);
            
            if(contract){
                contract.clientId = clientSaved.id;
                contract.client = clientSaved;
                await queryRunner.manager.save(contract);
            }

            let lead = await queryRunner.manager.findOne(Lead, client?.leads[0]?.id);
            if(lead){
                lead.clientId = clientSaved.id;
                lead.client = clientSaved;
                lead.gaint = true;
                await queryRunner.manager.save(lead);
            }

            await NodemailerProvider.registerMail(client.user.email.toLowerCase(), client.person.name, client.user.name, password);
        
            await queryRunner.commitTransaction();
            return clientSaved;

        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new AppError({ message: 'Erro ao transformar lead em cliente.' + error });

        } finally {
            await queryRunner.release();
        }
    }

    public async update(client: Client): Promise<Client | undefined>{
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const clientUpdated = await this.ormRepository.save(client);
        return clientUpdated;
    }

    public async delete(id: number): Promise<void> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        await this.ormRepository.delete(id);
    }

    private async verifyExistsUserWithSameName(name: string, email: string){
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.query(`SET SCHEMA 'public'`);
  
        let userNameArray = name.split(' ');
        let userName = `${userNameArray[0].toLowerCase()}.${userNameArray[1].toLowerCase()}`;
        let user = await queryRunner.manager.findOne(User, { name: userName });
        if(!user){
          return userName;
        }
  
        userNameArray = email.split('@');
        userName = userNameArray[0].toLowerCase();
        user = await queryRunner.manager.findOne(User, { name: userName });
        if(!user){
          return userName;
        }
  
        return email.toLowerCase();
    }

}

export default ClientRepository