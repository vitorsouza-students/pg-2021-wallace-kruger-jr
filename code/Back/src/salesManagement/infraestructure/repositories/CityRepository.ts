import { getConnection, getRepository, IsNull, Repository } from "typeorm";
import City from '../../domain/City';
import ICityRepository from "./interfaces/ICityRepository";


class CityRepository implements ICityRepository {

    private ormRepository: Repository<City>;
  
    constructor() {
        this.ormRepository = getRepository(City);
    }

    public async findCitysByUfUseCase(uf): Promise<City[] | undefined> {
        await this.ormRepository.query(`SET SCHEMA 'public'`);

        const city = this.ormRepository.find({ where: { uf }, order: { name: "ASC"} });
        return city;
    }

    public async findAllUfsUseCase(): Promise<string[] | undefined> {
        await this.ormRepository.query(`SET SCHEMA 'public'`);

        const ufs = await this.ormRepository.query(`
            SELECT DISTINCT(uf) FROM city
            ORDER BY uf ASC
        `);

        return ufs;
    }

}

export default CityRepository