import { getConnection, getRepository, IsNull, Repository } from "typeorm";
import ContractStatus from '../../domain/ContractStatus';
import IContractStatusRepository from "./interfaces/IContractStatusRepository";


class ContractStatusRepository implements IContractStatusRepository {

    private ormRepository: Repository<ContractStatus>;
  
    constructor(private tenant: string) {
        this.ormRepository = getRepository(ContractStatus);
    }

    public async findAll(): Promise<ContractStatus[] | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const contractStatus = this.ormRepository.find();
        return contractStatus;
    }

}

export default ContractStatusRepository