import { getConnection, getRepository, IsNull, Repository } from "typeorm";
import Contract from '../../domain/Contract';
import IContractRepository from "./interfaces/IContractRepository";


class ContractRepository implements IContractRepository {

    private ormRepository: Repository<Contract>;
  
    constructor(private tenant: string) {
        this.ormRepository = getRepository(Contract);
    }

    public async findById(id: number): Promise<Contract | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const contract = this.ormRepository.findOne({ where: { id }});
        return contract;
    }

    public async findByLeadUseCase(leadId: number): Promise<Contract | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const contract = await this.ormRepository.createQueryBuilder('contract')
            .innerJoinAndSelect('contract.contractStatus', 'contractStatus')
            .innerJoinAndSelect('contract.proposal', 'proposal')
            .where('proposal.lead_id = :leadId', { leadId })
            .getOne();

        return contract;
    }
    
    public async findAllContractsPaged(page: number, limit: number): Promise<[Contract[] | undefined, number]> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const offset = (page - 1) * limit;
        
        const [ contracts, count] = await this.ormRepository.createQueryBuilder('contract')
            .innerJoinAndSelect('contract.contractStatus', 'contractStatus')
            .innerJoinAndSelect('contract.client', 'client')
            .innerJoinAndSelect('client.person', 'person')
            .orderBy('contract.initialDate', 'DESC')
            .limit(limit)
            .offset(offset)
            .getManyAndCount();

        return [ contracts, count];
    }

    public async create(contract: Contract): Promise<Contract | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const contractCreated = await this.ormRepository.create(contract);
        const contractSaved = await this.ormRepository.save(contractCreated);

        return contractSaved;
    }

    public async update(contract: Contract): Promise<Contract | undefined>{
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const contractUpdated = await this.ormRepository.save(contract);
        return contractUpdated;
    }

    public async delete(id: number): Promise<void> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        await this.ormRepository.delete(id);
    }

}

export default ContractRepository