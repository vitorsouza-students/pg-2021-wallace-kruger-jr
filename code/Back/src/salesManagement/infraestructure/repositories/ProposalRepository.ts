import { getConnection, getRepository, IsNull, Repository } from "typeorm";
import Proposal from '../../domain/Proposal';
import IProposalRepository from "./interfaces/IProposalRepository";


class ProposalRepository implements IProposalRepository {

    private ormRepository: Repository<Proposal>;
  
    constructor(private tenant: string) {
        this.ormRepository = getRepository(Proposal);
    }

    public async findById(id: number): Promise<Proposal | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const proposal = this.ormRepository.findOne({ where: { id }});
        return proposal;
    }
    
    public async findAllProposalsPaged(page: number, limit: number): Promise<[Proposal[] | undefined, number]> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const offset = (page - 1) * limit;
        
        const [ proposals, count] = await this.ormRepository.createQueryBuilder('proposal')
            .orderBy('proposal.sendDate', 'DESC')
            .limit(limit)
            .offset(offset)
            .getManyAndCount();

        return [ proposals, count];
    }

    public async findByLeadUseCase(leadId: number): Promise<Proposal | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const proposal = this.ormRepository.findOne({ where: { leadId }});
        return proposal;        
    }

    public async create(proposal: Proposal): Promise<Proposal | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const proposalCreated = await this.ormRepository.create(proposal);
        const proposalSaved = await this.ormRepository.save(proposalCreated);

        return proposalSaved;
    }

    public async update(proposal: Proposal): Promise<Proposal | undefined>{
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const proposalUpdated = await this.ormRepository.save(proposal);
        return proposalUpdated;
    }

    public async delete(id: number): Promise<void> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        await this.ormRepository.delete(id);
    }

}

export default ProposalRepository