import City from '../../../domain/City';

export default interface ICityRepository {

    findCitysByUfUseCase(uf): Promise<City[] | undefined>;

    findAllUfsUseCase(): Promise<string[] | undefined>;

}