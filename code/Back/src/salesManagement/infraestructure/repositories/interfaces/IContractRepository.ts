import Contract from '../../../domain/Contract';

export default interface IContractRepository {

    findById(id: number): Promise<Contract | undefined>;

    findByLeadUseCase(leadId: number): Promise<Contract | undefined>;

    findAllContractsPaged(page: number, limit: number): Promise<[Contract[] | undefined, number]>;

    create(contract: Contract): Promise<Contract | undefined>;

    update(contract: Contract): Promise<Contract | undefined>;

    delete(id: number): Promise<void>;
}