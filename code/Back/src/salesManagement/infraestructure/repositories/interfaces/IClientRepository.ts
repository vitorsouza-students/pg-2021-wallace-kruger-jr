import Client from '../../../domain/Client';

export default interface IClientRepository {

    findById(id: number): Promise<Client | undefined>;

    findByEmail(email: string): Promise<Client | undefined>;

    findByCpf(cpf: string): Promise<Client | undefined>;

    findAllClientsPaged(page: number, limit: number): Promise<[Client[] | undefined, number]>;

    create(client: Client): Promise<Client | undefined>;

    update(client: Client): Promise<Client | undefined>;

    delete(id: number): Promise<void>;
}