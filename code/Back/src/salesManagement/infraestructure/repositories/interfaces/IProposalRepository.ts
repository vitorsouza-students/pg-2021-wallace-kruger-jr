import Proposal from '../../../domain/Proposal';

export default interface IProposalRepository {

    findById(id: number): Promise<Proposal | undefined>;

    findAllProposalsPaged(page: number, limit: number): Promise<[Proposal[] | undefined, number]>;

    findByLeadUseCase(leadId: number): Promise<Proposal | undefined>;

    create(proposal: Proposal): Promise<Proposal | undefined>;

    update(proposal: Proposal): Promise<Proposal | undefined>;

    delete(id: number): Promise<void>;
}