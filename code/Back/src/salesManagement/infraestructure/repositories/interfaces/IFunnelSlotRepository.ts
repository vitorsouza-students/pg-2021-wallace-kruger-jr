import FunnelSlot from "../../../domain/FunnelSlot";

export default interface IFunnelSlotRepository {

    findBySubsystem(subsystem: string): Promise<FunnelSlot[] | undefined>;
}