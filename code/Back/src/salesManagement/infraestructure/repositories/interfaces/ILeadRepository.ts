import Lead from '../../../domain/Lead';

export default interface ILeadRepository {

    findById(id: number): Promise<Lead | undefined>;

    findAllLeadsInTheFunnel(): Promise<Lead[] | undefined>;

    create(lead: Lead): Promise<Lead | undefined>;

    update(lead: Lead): Promise<Lead | undefined>;

    delete(id: number): Promise<void>;
}