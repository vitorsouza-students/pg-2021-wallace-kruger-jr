import ContractStatus from '../../../domain/ContractStatus';

export default interface IContractStatusRepository {

    findAll(): Promise<ContractStatus[] | undefined>;

}