import { getConnection, getRepository, Repository } from "typeorm";
import FunnelSlot from "../../domain/FunnelSlot";
import IFunnelSlotRepository from "./interfaces/IFunnelSlotRepository";


class FunnelSlotRepository implements IFunnelSlotRepository {

    private ormRepository: Repository<FunnelSlot>;
  
    constructor(private tenant: string) {
        this.ormRepository = getRepository(FunnelSlot);
    }
    
    public async findBySubsystem(subsystem: string): Promise<FunnelSlot[] | undefined> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);
        
        const [funnelSlots, count] = await this.ormRepository.createQueryBuilder('funnelSlot')
            .innerJoinAndSelect('funnelSlot.subsystem', 'subsystem')
            .where('subsystem.name = :name', { name: subsystem })
            .orderBy('funnelSlot.orderFunnel', 'ASC')
            .getManyAndCount();

        return funnelSlots;
    }

}

export default FunnelSlotRepository