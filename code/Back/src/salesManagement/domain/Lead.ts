import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Client from "./Client";
import FunnelSlot from "./FunnelSlot";
import Person from "./Person";
import Proposal from "./Proposal";
import StatusType from "./StatusType";

@Entity('lead')
class Lead {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('boolean')
  gaint: boolean;

  @Column('int4', { name: "funnel_slot_id" })
  funnelSlotId: number;

  @ManyToOne(type => FunnelSlot, funnelSlot => funnelSlot.leads, { eager: true })
  @JoinColumn({ name: "funnel_slot_id" })
  funnelSlot: FunnelSlot;

  @Column('int4', { name: "person_id" })
  personId: number;

  @OneToOne(type => Person, lead => Lead, { eager: true, cascade: true })
  @JoinColumn({ name: "person_id" })
  person: Person;

  @OneToMany(type => Proposal, proposal => Proposal)
  proposals: Proposal[];

  @Column('int4', { name: "client_id" })
  clientId: number;

  @ManyToOne(type => Client, client => client.leads, { eager: true })
  @JoinColumn({ name: "client_id" })
  client: Client;
}

export default Lead;