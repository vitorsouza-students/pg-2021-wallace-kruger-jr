import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Person from "./Person";

@Entity('contact')
class Contact {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  email: string;

  @Column('varchar')
  telephone: string;

  @Column('varchar')
  cellphone: string;

  @OneToOne(type => Person, contact => Contact)
  person: Person;
}

export default Contact;