import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import FunnelSlot from "./FunnelSlot";

@Entity('subsystem')
class Subsystem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  name: string;

  @OneToMany(type => FunnelSlot, funnelSlot => FunnelSlot)
  funnelSlots: FunnelSlot[];
}

export default Subsystem;