import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Lead from './Lead';
import Contract from './Contract';

@Entity('proposal')
class Proposal {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Generated('increment')
  code: string;

  @Column('text')
  description: string;

  @Column('timestamp', { name: "send_date" })
  sendDate: Date;

  @Column('numeric')
  value: number;

  @Column('int4', { name: "lead_id" })
  leadId: number;

  @ManyToOne(type => Lead, lead => lead.proposals, { eager: true })
  @JoinColumn({ name: "lead_id" })
  lead: Lead;

  @OneToOne(type => Contract, contract => Contract)
  contract: Contract;
}

export default Proposal;