import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import User from "../../userManagement/domain/User";
import Person from "./Person";
import Lead from './Lead';
import Contract from './Contract';

@Entity('client')
class Client {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column('varchar')
  @Generated('increment')
  code: string;

  @Column('int4', { name: "user_id" })
  userId?: number;

  @OneToOne(type => User, client => Client, { eager: true, cascade: true })
  @JoinColumn({ name: "user_id" })
  user: User;

  @Column('int4', { name: "person_id" })
  personId: number;

  @OneToOne(type => Person, client => Client, { eager: true, cascade: true })
  @JoinColumn({ name: "person_id" })
  person: Person;

  @OneToMany(type => Lead, lead => Lead)
  leads?: Lead[];

  @OneToMany(type => Contract, contract => Contract)
  contracts?: Contract[];
}

export default Client;