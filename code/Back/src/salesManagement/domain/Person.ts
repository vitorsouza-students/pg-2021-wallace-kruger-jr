import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Address from "./Address";
import Contact from "./Contact";
import Lead from "./Lead";
import Collaborator from '../../projectManagement/domain/Collaborator';
import Client from './Client';

@Entity('person')
class Person {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  name: string;

  @Column('varchar')
  cpf: string;

  @Column('int4', { name: "address_id" })
  addressId: number;

  @ManyToOne(type => Address, address => address.persons, { eager: true, cascade: true })
  @JoinColumn({ name: "address_id" })
  address: Address;

  @Column('int4', { name: "contact_id" })
  contactId: number;

  @OneToOne(type => Contact, person => Person, { eager: true, cascade: true })
  @JoinColumn({ name: "contact_id" })
  contact: Contact;

  @OneToOne(type => Lead, person => Person)
  lead: Lead;
  
  @OneToOne(type => Client, person => Person)
  client: Client;

  @OneToOne(type => Collaborator, person => Person)
  collaborator: Collaborator;
}

export default Person;