import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Contract from "./Contract";
import StatusType from "./StatusType";

@Entity('contract_status')
class ContractStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  name: string;

  @OneToMany(type => Contract, contract => Contract)
  contracts: Contract[];
}

export default ContractStatus;