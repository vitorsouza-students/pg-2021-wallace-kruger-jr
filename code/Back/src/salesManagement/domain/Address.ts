import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import City from "./City";
import Person from "./Person";

@Entity('address')
class Address {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { name: 'zip_code' })
  zipCode: string;

  @Column('varchar')
  street: string;

  @Column('varchar')
  neighborhood: string;

  @Column('varchar', { name: 'number_address' })
  numberAddress: string;

  @Column('varchar')
  complement: string;

  @Column('numeric')
  lat: number;

  @Column('numeric')
  lng: number;

  @Column('int4', { name: "city_id" })
  cityId: number;

  @ManyToOne(type => City, city => city.addresses, { eager: true })
  @JoinColumn({ name: "city_id" })
  city: City;

  @OneToMany(type => Person, person => Person)
  persons: Person[];
}

export default Address;