import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import Address from "./Address";

@Entity('public.city')
class City {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  name: string;

  @Column('varchar')
  uf: string;

  @OneToMany(type => Address, address => Address)
  addresses: Address[];
}

export default City;