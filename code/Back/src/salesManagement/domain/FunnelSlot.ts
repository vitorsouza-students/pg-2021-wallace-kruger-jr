import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Lead from "./Lead";
import Subsystem from "./Subsystem";

@Entity('funnel_slot')
class FunnelSlot {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  title: string;

  @Column('varchar')
  color: string;

  @Column('int', { name: "order_funnel" })
  orderFunnel: string;

  @Column('int4', { name: "subsystem_id" })
  subsystemId: number;

  @ManyToOne(type => Subsystem, subsystem => subsystem.funnelSlots)
  @JoinColumn({ name: "subsystem_id" })
  subsystem: Subsystem;

  @OneToMany(type => Lead, lead => Lead)
  leads: Lead[];
}

export default FunnelSlot;