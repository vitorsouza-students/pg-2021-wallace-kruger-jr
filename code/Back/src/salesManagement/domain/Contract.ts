import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Client from './Client';
import ContractStatus from "./ContractStatus";
import Proposal from "./Proposal";

@Entity('contract')
class Contract {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Generated('increment')
  code: string;

  @Column('timestamp', { name: "initial_date" })
  initialDate: string;

  @Column('timestamp', { name: "final_date" })
  finalDate: string;

  @Column('int4', { name: "proposal_id" })
  proposalId: number;

  @OneToOne(type => Proposal, contract => Contract, { eager: true, cascade: true })
  @JoinColumn({ name: "proposal_id" })
  proposal: Proposal;

  @Column('int4', { name: "contract_status_id" })
  contractStatusId: number;

  @ManyToOne(type => ContractStatus, contractStatus => contractStatus.contracts, { eager: true })
  @JoinColumn({ name: "contract_status_id" })
  contractStatus: ContractStatus;

  @Column('int4', { name: "client_id" })
  clientId: number;

  @ManyToOne(type => Client, client => client.contracts, { eager: true })
  @JoinColumn({ name: "client_id" })
  client: Client;
}

export default Contract;