import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('document')
class Document {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  name: string;

  @Column('varchar')
  nameS3: string;
}

export default Document;