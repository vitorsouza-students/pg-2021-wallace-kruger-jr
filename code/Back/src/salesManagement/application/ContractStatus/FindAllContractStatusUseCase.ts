import ContractStatus from "../../domain/ContractStatus";
import IContractStatusRepository from "../../infraestructure/repositories/interfaces/IContractStatusRepository";

class FindAllContractStatusUseCase {

    constructor(
        private contractStatusRepository: IContractStatusRepository,
    ) { }

    public async execute(): Promise<ContractStatus[] | undefined> {
        const contractStatus = await this.contractStatusRepository.findAll();
        
        return contractStatus;
    }
}

export default FindAllContractStatusUseCase;