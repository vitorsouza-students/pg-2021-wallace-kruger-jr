import City from "../../domain/City";
import ICityRepository from "../../infraestructure/repositories/interfaces/ICityRepository";

class FindAllUfsUseCase {

    constructor(
        private cityRepository: ICityRepository,
    ) { }

    public async execute() {
        const ufs = await this.cityRepository.findAllUfsUseCase();
        
        return ufs;
    }
}

export default FindAllUfsUseCase;