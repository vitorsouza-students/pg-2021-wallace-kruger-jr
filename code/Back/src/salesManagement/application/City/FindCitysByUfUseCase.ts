import City from "../../domain/City";
import ICityRepository from "../../infraestructure/repositories/interfaces/ICityRepository";

class FindCitysByUfUseCase {

    constructor(
        private cityRepository: ICityRepository,
    ) { }

    public async execute(uf: string): Promise<City[]> {
        const citys = await this.cityRepository.findCitysByUfUseCase(uf);
        
        return citys;
    }
}

export default FindCitysByUfUseCase;