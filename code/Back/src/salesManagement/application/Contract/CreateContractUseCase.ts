import Contract from "../../domain/Contract";
import IContractRepository from "../../infraestructure/repositories/interfaces/IContractRepository";

class CreateContractUseCase {

    constructor(
        private contractRepository: IContractRepository,
    ) { }

    public async execute(contract: Contract): Promise<Contract | undefined> {

        const contractSaved = await this.contractRepository.create(contract);
        
        return contractSaved;
    }
}

export default CreateContractUseCase;