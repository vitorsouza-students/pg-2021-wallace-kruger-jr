import Contract from "../../domain/Contract";
import ContractListDTO from "../../dtos/ContractListDTO";
import IContractRepository from "../../infraestructure/repositories/interfaces/IContractRepository";

interface Props {
    page: number,
    limit: number
  }

class FindAllContractsPagedUseCase {

    constructor(
        private contractRepository: IContractRepository,
    ) { }

    public async execute({ page, limit }: Props): Promise<[ContractListDTO[] | undefined, number]> {
        const contracts = await this.contractRepository.findAllContractsPaged(page, limit);
        
        return [
            contracts[0].map(contract => new ContractListDTO(contract)),
            contracts[1]
        ];
    }
}

export default FindAllContractsPagedUseCase;