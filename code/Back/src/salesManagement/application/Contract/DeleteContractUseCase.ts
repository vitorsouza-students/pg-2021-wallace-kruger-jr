import AppError from "../../../shared/errors/AppError";
import IContractRepository from "../../infraestructure/repositories/interfaces/IContractRepository";

class DeleteContractUseCase {

    constructor(
        private contractRepository: IContractRepository,
    ) { }

    async execute(id: number): Promise<void> {

        const contract = await this.contractRepository.findById(id);

        if(!contract) {
            throw new AppError({message: "Contrato não encontrado!", statusCode: 400, title: "Error! Não foi possível apagar!"});
        }

        await this.contractRepository.delete(id);
    }
}


export default DeleteContractUseCase;