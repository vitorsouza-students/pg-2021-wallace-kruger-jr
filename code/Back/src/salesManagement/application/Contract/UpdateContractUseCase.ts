import AppError from "../../../shared/errors/AppError";
import IContractRepository from "../../infraestructure/repositories/interfaces/IContractRepository";
import Contract from '../../domain/Contract';
import validator from "../../../shared/util/Validators";

class UpdateContractUseCase {

    constructor(
        private contractRepository: IContractRepository,
    ) { }

    async execute(contract: Contract): Promise<Contract | undefined> {

        let contractUpdating = await this.contractRepository.findById(contract.id);

        if(!contractUpdating) {
            throw new AppError({message: "Contrato não encontrado!", statusCode: 400, title: "Error! Não foi possível atualizar!"});
        }

        contractUpdating.initialDate = contract.initialDate;
        contractUpdating.finalDate = contract.finalDate;
        contractUpdating.contractStatusId = contract.contractStatusId;

        const contractUpdated = await this.contractRepository.update({ ... contractUpdating});
        
        return contractUpdated;
    }
}


export default UpdateContractUseCase;