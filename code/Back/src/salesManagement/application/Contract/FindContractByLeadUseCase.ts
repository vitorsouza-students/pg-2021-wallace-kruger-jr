import Contract from "../../domain/Contract";
import IContractRepository from "../../infraestructure/repositories/interfaces/IContractRepository";


class FindContractByLeadUseCase {

    constructor(
        private contractRepository: IContractRepository,
    ) { }

    public async execute(id: number): Promise<Contract | undefined> {
        const contract = await this.contractRepository.findByLeadUseCase(id);
        
        return contract;
    }
}

export default FindContractByLeadUseCase;