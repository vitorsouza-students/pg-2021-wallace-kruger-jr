import FunnelSlotDTO from "../../dtos/FunnelSlotDTO";
import IFunnelSlotRepository from "../../infraestructure/repositories/interfaces/IFunnelSlotRepository";

class FindBySubsystemUseCase {

    constructor(
        private funnelSlotRepository: IFunnelSlotRepository,
    ) { }

    public async execute(subsystem: string) {

        const funnelSlots = await this.funnelSlotRepository.findBySubsystem(subsystem);

        return funnelSlots.map(funnelSlot => new FunnelSlotDTO(funnelSlot));
    }
}

export default FindBySubsystemUseCase;