import AppError from "../../../shared/errors/AppError";
import Client from "../../domain/Client";
import IClientRepository from "../../infraestructure/repositories/interfaces/IClientRepository";
import validator from "../../../shared/util/Validators";

class CreateClientUseCase {

    constructor(
        private clientRepository: IClientRepository,
    ) { }

    public async execute(client: Client): Promise<Client | undefined> {

        client.person.cpf = client.person.cpf.replace(/[\.\-\/]/g, '');
        this.verifyCpfValid(client.person.cpf);
        await this.verifyExistsClientWithSameCpf(client.person.cpf);

        this.verifyEmailValid(client.user.email);
        await this.verifyExistsClientWithSameEmail(client.user.email);        

        const clientSaved = await this.clientRepository.create(client);
        
        return clientSaved;
    }

    private verifyEmailValid(email: string) {
        if (!email) {
            return;
        }
        const validateEmail = validator.verifyEmail(email);

        if (!validateEmail.valid) {
            throw new AppError({ message: validateEmail.response });
        }
    }

    private async verifyExistsClientWithSameEmail(email: string) {
        const client = await this.clientRepository.findByEmail(email);

        if (client) {
            throw new AppError({ message: 'Este e-mail já está sendo usado por um cliente.' });
        }
    }

    private verifyCpfValid(cpf: string) {
        if (!cpf) {
            return;
        }
        const validatePatientDoc = validator.validateCpf(cpf);

        if (!validatePatientDoc.valid) {
            throw new AppError({ message: validatePatientDoc.response });
        }
    }

    private async verifyExistsClientWithSameCpf(cpf: string) {
        const client = await this.clientRepository.findByCpf(cpf);

        if (client) {
            throw new AppError({ message: 'Este CPF já está sendo usado por um cliente.' });
        }
    }
}

export default CreateClientUseCase;