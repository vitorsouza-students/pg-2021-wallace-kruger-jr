import AppError from "../../../shared/errors/AppError";
import IClientRepository from "../../infraestructure/repositories/interfaces/IClientRepository";
import Client from '../../domain/Client';
import validator from "../../../shared/util/Validators";

class UpdateClientUseCase {

    constructor(
        private clientRepository: IClientRepository,
    ) { }

    async execute(client: Client): Promise<Client | undefined> {

        let clientUpdating = await this.clientRepository.findById(client.id);

        if(!clientUpdating) {
            throw new AppError({message: "Cliente não encontrado!", statusCode: 400, title: "Error! Não foi possível atualizar!"});
        }

        this.verifyEmailValid(client.person.contact.email);

        this.verifyCpfValid(client.person.cpf);

        clientUpdating.person.name = client.person.name;
        clientUpdating.person.cpf = client.person.cpf;
        clientUpdating.person.contact.email = client.person.contact.email;
        clientUpdating.person.contact.telephone = client.person.contact.telephone;
        clientUpdating.person.contact.cellphone = client.person.contact.cellphone;
        clientUpdating.person.address.zipCode = client.person.address.zipCode;
        clientUpdating.person.address.street = client.person.address.street;
        clientUpdating.person.address.neighborhood = client.person.address.neighborhood;
        clientUpdating.person.address.numberAddress = client.person.address.numberAddress;
        clientUpdating.person.address.complement = client.person.address.complement;
        clientUpdating.person.address.lat = client.person.address.lat;
        clientUpdating.person.address.lng = client.person.address.lng;

        const clientUpdated = await this.clientRepository.update({ ... clientUpdating});
        
        return clientUpdated;
    }

    private verifyEmailValid(email: string) {
        if (!email) {
            return;
        }
        const validateEmail = validator.verifyEmail(email);

        if (!validateEmail.valid) {
            throw new AppError({ message: validateEmail.response });
        }
    }

    private verifyCpfValid(cpf: string) {
        if (!cpf) {
            return;
        }
        const validatePatientDoc = validator.validateCpf(cpf);

        if (!validatePatientDoc.valid) {
            throw new AppError({ message: validatePatientDoc.response });
        }
    }
}


export default UpdateClientUseCase;