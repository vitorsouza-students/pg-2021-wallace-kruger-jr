import AppError from "../../../shared/errors/AppError";
import IClientRepository from "../../infraestructure/repositories/interfaces/IClientRepository";

class DeleteClientUseCase {

    constructor(
        private clientRepository: IClientRepository,
    ) { }

    async execute(id: number): Promise<void> {

        const client = await this.clientRepository.findById(id);

        if(!client) {
            throw new AppError({message: "Cliente não encontrado!", statusCode: 400, title: "Error! Não foi possível apagar!"});
        }

        await this.clientRepository.delete(id);
    }
}


export default DeleteClientUseCase;