import Client from "../../domain/Client";
import ClientDTO from "../../dtos/ClientDTO";
import IClientRepository from "../../infraestructure/repositories/interfaces/IClientRepository";

interface Props {
    page: number,
    limit: number
  }

class FindAllClientsPagedUseCase {

    constructor(
        private clientRepository: IClientRepository,
    ) { }

    public async execute({ page, limit }: Props): Promise<[ClientDTO[] | undefined, number]> {
        const clients = await this.clientRepository.findAllClientsPaged(page, limit);
        
        return [ 
            clients[0].map(client => new ClientDTO(client)),
            clients[1]
            ];
    }
}

export default FindAllClientsPagedUseCase;