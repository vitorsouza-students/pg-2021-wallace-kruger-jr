import AppError from "../../../shared/errors/AppError";
import IProposalRepository from "../../infraestructure/repositories/interfaces/IProposalRepository";
import Proposal from '../../domain/Proposal';
import validator from "../../../shared/util/Validators";

class UpdateProposalUseCase {

    constructor(
        private proposalRepository: IProposalRepository,
    ) { }

    async execute(proposal: Proposal): Promise<Proposal | undefined> {

        let proposalUpdating = await this.proposalRepository.findById(proposal.id);

        if(!proposalUpdating) {
            throw new AppError({message: "Proposata não encontrada!", statusCode: 400, title: "Error! Não foi possível atualizar!"});
        }

        proposalUpdating.description = proposal.description;
        proposalUpdating.sendDate = proposal.sendDate;
        proposalUpdating.value = proposal.value;

        const proposalUpdated = await this.proposalRepository.update({ ... proposalUpdating});
        
        return proposalUpdated;
    }
}


export default UpdateProposalUseCase;