import Proposal from "../../domain/Proposal";
import IProposalRepository from "../../infraestructure/repositories/interfaces/IProposalRepository";


class FindProposalByLeadUseCase {

    constructor(
        private proposalRepository: IProposalRepository,
    ) { }

    public async execute(id: number): Promise<Proposal | undefined> {
        const proposal = await this.proposalRepository.findByLeadUseCase(id);
        
        return proposal;
    }
}

export default FindProposalByLeadUseCase;