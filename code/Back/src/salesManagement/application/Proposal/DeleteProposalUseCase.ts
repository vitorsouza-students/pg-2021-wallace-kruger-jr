import AppError from "../../../shared/errors/AppError";
import IProposalRepository from "../../infraestructure/repositories/interfaces/IProposalRepository";

class DeleteProposalUseCase {

    constructor(
        private proposalRepository: IProposalRepository,
    ) { }

    async execute(id: number): Promise<void> {

        const proposal = await this.proposalRepository.findById(id);

        if(!proposal) {
            throw new AppError({message: "Proposta não encontrada!", statusCode: 400, title: "Error! Não foi possível apagar!"});
        }

        await this.proposalRepository.delete(id);
    }
}


export default DeleteProposalUseCase;