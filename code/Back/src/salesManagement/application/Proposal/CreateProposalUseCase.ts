import Proposal from "../../domain/Proposal";
import IProposalRepository from "../../infraestructure/repositories/interfaces/IProposalRepository";

class CreateProposalUseCase {

    constructor(
        private proposalRepository: IProposalRepository,
    ) { }

    public async execute(proposal: Proposal): Promise<Proposal | undefined> {

        const proposalSaved = await this.proposalRepository.create(proposal);
        
        return proposalSaved;
    }
}

export default CreateProposalUseCase;