import Proposal from "../../domain/Proposal";
import IProposalRepository from "../../infraestructure/repositories/interfaces/IProposalRepository";

interface Props {
    page: number,
    limit: number
  }

class FindAllProposalsPagedUseCase {

    constructor(
        private proposalRepository: IProposalRepository,
    ) { }

    public async execute({ page, limit }: Props): Promise<[Proposal[] | undefined, number]> {
        const proposals = await this.proposalRepository.findAllProposalsPaged(page, limit);
        
        return proposals;
    }
}

export default FindAllProposalsPagedUseCase;