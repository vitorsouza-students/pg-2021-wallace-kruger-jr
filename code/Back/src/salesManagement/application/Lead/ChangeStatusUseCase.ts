import AppError from "../../../shared/errors/AppError";
import ILeadRepository from "../../infraestructure/repositories/interfaces/ILeadRepository";
import Lead from '../../domain/Lead';

class ChangeStatusUseCase {

    constructor(
        private leadRepository: ILeadRepository,
    ) { }

    async execute(lead: Lead): Promise<Lead | undefined> {

        let leadUpdating = await this.leadRepository.findById(lead.id);

        if(!leadUpdating) {
            throw new AppError({message: "Lead não encontrada!", statusCode: 400, title: "Error! Não foi possível atualizar!"});
        }

        leadUpdating.funnelSlotId = lead.funnelSlotId;
        leadUpdating.funnelSlot.id = lead.funnelSlotId;

        const leadUpdated = await this.leadRepository.update({ ... leadUpdating});
        
        return leadUpdated;
    }
}


export default ChangeStatusUseCase;