import AppError from "../../../shared/errors/AppError";
import Lead from "../../domain/Lead";
import ILeadRepository from "../../infraestructure/repositories/interfaces/ILeadRepository";
import validator from "../../../shared/util/Validators";

class CreateLeadUseCase {

    constructor(
        private leadRepository: ILeadRepository,
    ) { }

    public async execute(lead: Lead): Promise<Lead | undefined> {

        this.verifyEmailValid(lead.person.contact.email);

        this.verifyCpfValid(lead.person.cpf);

        const leadSaved = await this.leadRepository.create(lead);
        
        return leadSaved;
    }

    private verifyEmailValid(email: string) {
        if (!email) {
            return;
        }
        const validateEmail = validator.verifyEmail(email);

        if (!validateEmail.valid) {
            throw new AppError({ message: validateEmail.response });
        }
    }

    private verifyCpfValid(cpf: string) {
        if (!cpf) {
            return;
        }
        const validatePatientDoc = validator.validateCpf(cpf);

        if (!validatePatientDoc.valid) {
            throw new AppError({ message: validatePatientDoc.response });
        }
    }
}

export default CreateLeadUseCase;