import AppError from "../../../shared/errors/AppError";
import ILeadRepository from "../../infraestructure/repositories/interfaces/ILeadRepository";

class DeleteLeadUseCase {

    constructor(
        private leadRepository: ILeadRepository,
    ) { }

    async execute(id: number): Promise<void> {

        const lead = await this.leadRepository.findById(id);

        if(!lead) {
            throw new AppError({message: "Lead não encontrada!", statusCode: 400, title: "Error! Não foi possível apagar!"});
        }

        await this.leadRepository.delete(id);
    }
}


export default DeleteLeadUseCase;