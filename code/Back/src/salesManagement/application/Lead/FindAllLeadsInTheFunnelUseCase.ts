import LeadsInFunnelDTO from "../../dtos/LeadsInFunnelDTO";
import ILeadRepository from "../../infraestructure/repositories/interfaces/ILeadRepository";

class FindAllLeadsInTheFunnelUseCase {

    constructor(
        private leadRepository: ILeadRepository,
    ) { }

    public async execute(): Promise<LeadsInFunnelDTO[] | undefined> {
        const leads = await this.leadRepository.findAllLeadsInTheFunnel();
        
        return leads.map(lead => new LeadsInFunnelDTO(lead));
    }
}

export default FindAllLeadsInTheFunnelUseCase;