import AppError from "../../../shared/errors/AppError";
import ILeadRepository from "../../infraestructure/repositories/interfaces/ILeadRepository";
import Lead from '../../domain/Lead';
import validator from "../../../shared/util/Validators";

class UpdateLeadUseCase {

    constructor(
        private leadRepository: ILeadRepository,
    ) { }

    async execute(lead: Lead): Promise<Lead | undefined> {

        let leadUpdating = await this.leadRepository.findById(lead.id);

        if(!leadUpdating) {
            throw new AppError({message: "Lead não encontrada!", statusCode: 400, title: "Error! Não foi possível atualizar!"});
        }

        this.verifyEmailValid(lead.person.contact.email);

        this.verifyCpfValid(lead.person.cpf);

        leadUpdating.person.name = lead.person.name;
        leadUpdating.person.cpf = lead.person.cpf;
        leadUpdating.person.contact.email = lead.person.contact.email;
        leadUpdating.person.contact.telephone = lead.person.contact.telephone;
        leadUpdating.person.contact.cellphone = lead.person.contact.cellphone;

        const leadUpdated = await this.leadRepository.update({ ... leadUpdating});
        
        return leadUpdated;
    }

    private verifyEmailValid(email: string) {
        if (!email) {
            return;
        }
        const validateEmail = validator.verifyEmail(email);

        if (!validateEmail.valid) {
            throw new AppError({ message: validateEmail.response });
        }
    }

    private verifyCpfValid(cpf: string) {
        if (!cpf) {
            return;
        }
        const validatePatientDoc = validator.validateCpf(cpf);

        if (!validatePatientDoc.valid) {
            throw new AppError({ message: validatePatientDoc.response });
        }
    }
}


export default UpdateLeadUseCase;