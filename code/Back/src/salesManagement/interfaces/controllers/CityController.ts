import { Request, Response } from 'express';
import FindAllUfsUseCase from '../../application/City/FindAllUfsUseCase';
import FindCitysByUfUseCase from '../../application/City/FindCitysByUfUseCase';
import CityRepository from '../../infraestructure/repositories/CityRepository';

export class CityController {

  public async findCitysByUf(request: Request, response: Response) {
    const cityRepository = new CityRepository();

    try {
        const { uf } = request.params;

        const findCitysByUfUseCase = new FindCitysByUfUseCase(cityRepository);
        const citys = await findCitysByUfUseCase.execute(uf);

        return response.status(200).json(citys);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async findUfs(request: Request, response: Response) {
    const cityRepository = new CityRepository();

    try {
        const findAllUfsUseCase = new FindAllUfsUseCase(cityRepository);
        const ufs = await findAllUfsUseCase.execute();

        return response.status(200).json(ufs);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

}