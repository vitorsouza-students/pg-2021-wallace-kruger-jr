import { Request, Response } from 'express';
import FindBySubsystemUseCase from '../../application/FunnelSlot/FindBySubsystemUseCase';
import FunnelSlotRepository from '../../infraestructure/repositories/FunnelSlotRepository';

export class FunnelSlotController {

  public async find(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const funnelSlotRepository = new FunnelSlotRepository(tenant);

    try {
        const { subsystem } = request.params;

        const findBySubsystemUseCase = new FindBySubsystemUseCase(funnelSlotRepository);
        const funnelSlots = await findBySubsystemUseCase.execute(subsystem);

        return response.status(200).json(funnelSlots);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

}