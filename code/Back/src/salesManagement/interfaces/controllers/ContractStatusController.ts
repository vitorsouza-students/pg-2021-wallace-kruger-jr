import { Request, Response } from 'express';
import FindAllContractStatusUseCase from '../../application/ContractStatus/FindAllContractStatusUseCase';
import ContractStatusRepository from '../../infraestructure/repositories/ContractStatusRepository';

export class ContractStatusController {

  public async findAll(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const contractStatusRepository = new ContractStatusRepository(tenant);

    try {
        const findAllContractStatussPagedUseCase = new FindAllContractStatusUseCase(contractStatusRepository);
        const contractStatus = await findAllContractStatussPagedUseCase.execute();

        return response.status(200).json(contractStatus);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

}