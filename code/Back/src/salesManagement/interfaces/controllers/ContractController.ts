import { Request, Response } from 'express';
import CreateContractUseCase from '../../application/Contract/CreateContractUseCase';
import DeleteContractUseCase from '../../application/Contract/DeleteContractUseCase';
import FindAllContractsPagedUseCase from '../../application/Contract/FindAllContractsPagedUseCase';
import FindContractByLeadUseCase from '../../application/Contract/FindContractByLeadUseCase';
import UpdateContractUseCase from '../../application/Contract/UpdateContractUseCase';
import ContractRepository from '../../infraestructure/repositories/ContractRepository';

export class ContractController {

  public async findAll(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const contractRepository = new ContractRepository(tenant);

    try {
        const { page, limit } = request.query;
        const getPage = page || 1;
        const getLimit = limit || 10;

        const findAllContractsPagedUseCase = new FindAllContractsPagedUseCase(contractRepository);
        const contracts = await findAllContractsPagedUseCase.execute({
            page: +getPage,
            limit: +getLimit
        });

        return response.status(200).json(contracts);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async findByLead(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const contractRepository = new ContractRepository(tenant);

    try {
        const { id } = request.params;

        const findContractByLeadUseCase = new FindContractByLeadUseCase(contractRepository);
        const contract = await findContractByLeadUseCase.execute(+id);

        return response.status(200).json(contract);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async create(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const contractRepository = new ContractRepository(tenant);

    try {
        const contract = request.body;

        const createContractUseCase = new CreateContractUseCase(contractRepository);
        const contracts = await createContractUseCase.execute(contract);

        return response.status(201).json(contracts);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async update(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const contractRepository = new ContractRepository(tenant);

    try {
        const contract = request.body;

        const updateContractUseCase = new UpdateContractUseCase(contractRepository);
        const contracts = await updateContractUseCase.execute(contract);

        return response.status(200).json(contracts);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async delete(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const contractRepository = new ContractRepository(tenant);

    try {
        const { id } = request.params;

        const deleteContractUseCase = new DeleteContractUseCase(contractRepository);
        await deleteContractUseCase.execute(+id);

        return response.status(200).send();
    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
}

}