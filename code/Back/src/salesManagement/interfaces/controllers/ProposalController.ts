import { Request, Response } from 'express';
import CreateProposalUseCase from '../../application/Proposal/CreateProposalUseCase';
import DeleteProposalUseCase from '../../application/Proposal/DeleteProposalUseCase';
import FindAllProposalsPagedUseCase from '../../application/Proposal/FindAllProposalsPagedUseCase';
import FindProposalByLeadUseCase from '../../application/Proposal/FindProposalByLeadUseCase';
import UpdateProposalUseCase from '../../application/Proposal/UpdateProposalUseCase';
import ProposalRepository from '../../infraestructure/repositories/ProposalRepository';

export class ProposalController {

  public async findAll(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const proposalRepository = new ProposalRepository(tenant);

    try {
        const { page, limit } = request.query;
        const getPage = page || 1;
        const getLimit = limit || 10;

        const findAllProposalsPagedUseCase = new FindAllProposalsPagedUseCase(proposalRepository);
        const proposals = await findAllProposalsPagedUseCase.execute({
            page: +getPage,
            limit: +getLimit
        });

        return response.status(200).json(proposals);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async findByLead(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const proposalRepository = new ProposalRepository(tenant);

    try {
        const { id } = request.params;

        const findProposalByLeadUseCase = new FindProposalByLeadUseCase(proposalRepository);
        const proposal = await findProposalByLeadUseCase.execute(+id);

        return response.status(200).json(proposal);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async create(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const proposalRepository = new ProposalRepository(tenant);

    try {
        const proposal = request.body;

        const createProposalUseCase = new CreateProposalUseCase(proposalRepository);
        const proposals = await createProposalUseCase.execute(proposal);

        return response.status(201).json(proposals);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async update(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const proposalRepository = new ProposalRepository(tenant);

    try {
        const proposal = request.body;

        const updateProposalUseCase = new UpdateProposalUseCase(proposalRepository);
        const proposals = await updateProposalUseCase.execute(proposal);

        return response.status(200).json(proposals);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async delete(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const proposalRepository = new ProposalRepository(tenant);

    try {
        const { id } = request.params;

        const deleteProposalUseCase = new DeleteProposalUseCase(proposalRepository);
        await deleteProposalUseCase.execute(+id);

        return response.status(200).send();
    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
}

}