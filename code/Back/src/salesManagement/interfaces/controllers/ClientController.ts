import { Request, Response } from 'express';
import CreateClientUseCase from '../../application/Client/CreateClientUseCase';
import DeleteClientUseCase from '../../application/Client/DeleteClientUseCase';
import FindAllClientsPagedUseCase from '../../application/Client/FindAllClientsPagedUseCase';
import UpdateClientUseCase from '../../application/Client/UpdateClientUseCase';
import ClientRepository from '../../infraestructure/repositories/ClientRepository';

export class ClientController {

  public async findAll(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const clientRepository = new ClientRepository(tenant);

    try {
        const { page, limit } = request.query;
        const getPage = page || 1;
        const getLimit = limit || 10;

        const findAllClientsPagedUseCase = new FindAllClientsPagedUseCase(clientRepository);
        const clients = await findAllClientsPagedUseCase.execute({
            page: +getPage,
            limit: +getLimit
        });

        return response.status(200).json(clients);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async create(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const clientRepository = new ClientRepository(tenant);

    try {
        const client = request.body;

        const createClientUseCase = new CreateClientUseCase(clientRepository);
        const clients = await createClientUseCase.execute(client);

        return response.status(201).json(clients);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async update(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const clientRepository = new ClientRepository(tenant);

    try {
        const client = request.body;

        const updateClientUseCase = new UpdateClientUseCase(clientRepository);
        const clients = await updateClientUseCase.execute(client);

        return response.status(200).json(clients);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async delete(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const clientRepository = new ClientRepository(tenant);

    try {
        const { id } = request.params;

        const deleteClientUseCase = new DeleteClientUseCase(clientRepository);
        await deleteClientUseCase.execute(+id);

        return response.status(200).send();
    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
}

}