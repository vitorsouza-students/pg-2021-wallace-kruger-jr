import { Request, Response } from 'express';
import ChangeStatusUseCase from '../../application/Lead/ChangeStatusUseCase';
import CreateLeadUseCase from '../../application/Lead/CreateLeadUseCase';
import DeclareWinLossUseCase from '../../application/Lead/DeclareWinLossUseCase';
import DeleteLeadUseCase from '../../application/Lead/DeleteLeadUseCase';
import FindAllLeadsInTheFunnelUseCase from '../../application/Lead/FindAllLeadsInTheFunnelUseCase';
import UpdateLeadUseCase from '../../application/Lead/UpdateLeadUseCase';
import LeadRepository from '../../infraestructure/repositories/LeadRepository';

export class LeadController {

  public async findAll(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const leadRepository = new LeadRepository(tenant);

    try {

        const findAllLeadsInTheFunnelUseCase = new FindAllLeadsInTheFunnelUseCase(leadRepository);
        const leads = await findAllLeadsInTheFunnelUseCase.execute();

        return response.status(200).json(leads);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async create(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const leadRepository = new LeadRepository(tenant);

    try {
        const lead = request.body;

        const createLeadUseCase = new CreateLeadUseCase(leadRepository);
        const leads = await createLeadUseCase.execute(lead);

        return response.status(201).json(leads);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async update(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const leadRepository = new LeadRepository(tenant);

    try {
        const lead = request.body;

        const updateLeadUseCase = new UpdateLeadUseCase(leadRepository);
        const leads = await updateLeadUseCase.execute(lead);

        return response.status(200).json(leads);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async changeStatus(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const leadRepository = new LeadRepository(tenant);

    try {
        const lead = request.body;

        const changeStatusUseCase = new ChangeStatusUseCase(leadRepository);
        const leads = await changeStatusUseCase.execute(lead);

        return response.status(200).json(leads);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async declareWinLoss(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const leadRepository = new LeadRepository(tenant);

    try {
        const lead = request.body;

        const declareWinLossUseCase = new DeclareWinLossUseCase(leadRepository);
        const leads = await declareWinLossUseCase.execute(lead);

        return response.status(200).json(leads);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async delete(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
    const leadRepository = new LeadRepository(tenant);

    try {
        const { id } = request.params;

        const deleteLeadUseCase = new DeleteLeadUseCase(leadRepository);
        await deleteLeadUseCase.execute(+id);

        return response.status(200).send();
    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
}

}