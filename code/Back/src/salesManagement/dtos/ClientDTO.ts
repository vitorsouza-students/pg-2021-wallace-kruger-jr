import Client from "../domain/Client";

class ClientDTO {
    id: number;
    code: string;
    personName: string;
    personCpf: string;
    personContactEmail: string;
    personContactCellphone: string;
    addressNeighborhood: string;
    addressCityName: string;
    addressCityUf: string;

    constructor(client: Client) {
        this.id = client.id;
        this.code = client.code;
        this.personName = client.person.name;
        this.personCpf = client.person.cpf;
        this.personContactEmail = client.person.contact.email;
        this.personContactCellphone = client.person.contact.cellphone;
        this.addressNeighborhood = client.person.address.neighborhood;
        this.addressCityName = client.person.address.city.name;
        this.addressCityUf = client.person.address.city.uf;
    }
}

export default ClientDTO;