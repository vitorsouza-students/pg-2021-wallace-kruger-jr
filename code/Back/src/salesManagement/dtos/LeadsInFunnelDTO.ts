import Lead from '../domain/Lead';
class LeadsInFunnelDTO {
    id: number;
    funnelSlotId: string;
    personName: string;
    personCpf!: string;
    personEmail!: string | undefined;
    personCellphone!: string | undefined;
    personInfo: string;

    constructor(lead: Lead) {
        this.id = lead.id;
        this.funnelSlotId = lead.funnelSlotId.toString();
        this.personName = lead.person.name;
        this.personCpf = lead.person.cpf;
        this.personEmail = lead.person?.contact?.email;
        this.personCellphone = lead.person?.contact?.cellphone;
        this.personInfo = `Celular: ${lead.person.contact.cellphone}, E-mail: ${lead.person.contact.email}`;
    }
}

export default LeadsInFunnelDTO;