import Contract from "../domain/Contract";

class ContractListDTO {
    id: number;
    code: string;
    initialDate: string;
    finalDate: string;
    clientPersonName: string;
    contractStatusName: string;

    constructor(contract: Contract) {
        this.id = contract.id;
        this.code = contract.code;
        this.initialDate = contract.initialDate;
        this.finalDate = contract.finalDate;
        this.clientPersonName = contract.client.person.name;
        this.contractStatusName = contract.contractStatus.name;
    }
}

export default ContractListDTO;