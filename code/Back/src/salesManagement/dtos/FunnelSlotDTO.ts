import FunnelSlot from "../domain/FunnelSlot";

class FunnelSlotDTO {
    id: string;
    title: string;
    color: string;

    constructor(slotFunnel: FunnelSlot) {
        this.id = slotFunnel.id.toString();
        this.title = slotFunnel.title;
        this.color = slotFunnel.color;
    }
}

export default FunnelSlotDTO;