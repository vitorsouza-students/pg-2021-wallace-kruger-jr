import { Request, Response } from 'express';
import CreateOrganizationUseCase from '../../application/CreateOrganizationUseCase';
import OrganizationRepository from '../../infraestructure/repositories/OrganizationRepository';

export class OrganizationController {

  public async create(request: Request, response: Response) {
    const organizationRepository = new OrganizationRepository();

    try {
      const [ organization, collaborator ] = request.body;

      const organizationCreateUseCase = new CreateOrganizationUseCase(organizationRepository);
      const organizationDTO = await organizationCreateUseCase.execute(organization, collaborator);

      return response.status(201).json(organizationDTO);

    } catch (err) {
      return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

}