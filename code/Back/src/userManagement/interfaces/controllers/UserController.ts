import { Request, Response } from 'express';
import UserRepository from '../../../userManagement/infraestructure/repositories/UserRepository';
import TenantRepository from '../../infraestructure/repositories/TenantRepository';
import ForgotPasswordUserUseCase from '../../application/ForgotPasswordUserUseCase';
import ResetPasswordUserUseCase from '../../application/ResetPasswordUserUseCase';
import ChangePasswordUserUseCase from '../../application/ChangePasswordUserUseCase';
import AuthenticateUserUseCase from '../../application/AuthenticateUserUseCase';

export class UserController {

  public async login(request: Request, response: Response) {
    const userRepository = new UserRepository();
    const tenantRepository = new TenantRepository();

    try {
      const login = request.body;

      const authenticateUserUseCase = new AuthenticateUserUseCase(userRepository, tenantRepository);
      const token = await authenticateUserUseCase.execute(login.name, login.password);

      return response.status(200).json(token);

    } catch (err) {
      return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async forgotPassword(request: Request, response: Response) {
    const userRepository = new UserRepository();

    try {
      const forgotPassword = request.body;

      const forgotPasswordUserUseCase = new ForgotPasswordUserUseCase(userRepository);
      await forgotPasswordUserUseCase.execute(forgotPassword.email);

      return response.status(204).json();

    } catch (err) {
      return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async resetPassword(request: Request, response: Response) {
    const userRepository = new UserRepository();

    try {
      const resetPassword = request.body;

      const resetPasswordUserUseCase = new ResetPasswordUserUseCase(userRepository);
      await resetPasswordUserUseCase.execute(resetPassword.id, resetPassword.newPassword);

      return response.status(204).json();

    } catch (err) {
      return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

  public async changePassword(request: Request, response: Response) {
    const userRepository = new UserRepository();

    try {
      const changePassword = request.body;

      const changePasswordUserUseCase = new ChangePasswordUserUseCase(userRepository);
      await changePasswordUserUseCase.execute(changePassword.id, changePassword.password, changePassword.newPassword);

      return response.status(204).json();

    } catch (err) {
      return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

}