import { getConnection, getRepository, Repository } from "typeorm";
import AppError from "../../../shared/errors/AppError";
import User from "../../domain/User";
import IUserRepository from "./interfaces/IUserRepository";


class UserRepository implements IUserRepository {

    private ormRepository: Repository<User>;
  
    constructor() {
        this.ormRepository = getRepository(User);
    }

    public async findById(id: number): Promise<User | undefined> {
        await this.ormRepository.query(`SET SCHEMA 'public'`);
        const user = await this.ormRepository.findOne({ where: { id } });
        return user;
    }
    
    public async findByName(name: string): Promise<User | undefined> {
        await this.ormRepository.query(`SET SCHEMA 'public'`);
        const user = await this.ormRepository.findOne({ where: { name } });
        return user;
    }

    public async findByEmail(email: string): Promise<User | undefined> {
        await this.ormRepository.query(`SET SCHEMA 'public'`);
        const user = await this.ormRepository.findOne({ where: { email } });
        return user;
    }

    public async update(user: User): Promise<User> {
        await this.ormRepository.query(`SET SCHEMA 'public'`);
        const userUpdated = await this.ormRepository.save(user);
        return userUpdated;
    }

}

export default UserRepository