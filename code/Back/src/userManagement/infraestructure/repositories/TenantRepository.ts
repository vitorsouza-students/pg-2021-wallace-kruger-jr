import { getConnection, getRepository, Repository } from "typeorm";
import Tenant from "../../domain/Tenant";
import ITenantRepository from "./interfaces/ITenantRepository";


class UserRepository implements ITenantRepository {

    private ormRepository: Repository<Tenant>;
  
    constructor() {
        this.ormRepository = getRepository(Tenant);
    }
    
    public async findByOrganization(organizationId: number): Promise<Tenant | undefined> {
        await this.ormRepository.query(`SET SCHEMA 'public'`);
        const tenant = await this.ormRepository.findOne({ where: { organizationId } });
        return tenant;
    }

}

export default UserRepository