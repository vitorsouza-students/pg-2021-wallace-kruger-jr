import { getConnection, getRepository, Repository } from "typeorm";
import crypto from 'crypto';
import fs from "fs";
import path from "path";
import { hash } from 'bcryptjs'; 
import Organization from "../../../userManagement/domain/Organization";
import AppError from "../../../shared/errors/AppError";
import IOrganizationRepository from "./interfaces/IOrganizationRepository";
import NodemailerProvider from "../../../shared/providers/Mail/NodemailerProvider";
import Collaborator from "../../../projectManagement/domain/Collaborator";
import Tenant from "../../domain/Tenant";
import User from "../../domain/User";


class OrganizationRepository implements IOrganizationRepository {

    private ormRepository: Repository<Organization>;
  
    constructor() {
        this.ormRepository = getRepository(Organization);
    }

    public async findByName(organizationName: string): Promise<Organization> {
        await this.ormRepository.query(`SET SCHEMA 'public'`);
        const organization = await this.ormRepository.findOne({ where: { name: organizationName }, });
        return organization;
    }
    
    public async findByCnpj(cnpj: string): Promise<Organization | undefined> {
        await this.ormRepository.query(`SET SCHEMA 'public'`);
        const organization = await this.ormRepository.findOne({ where: { cnpj: cnpj } });
        return organization;
    }

    public async create(organization: Organization, collaborator: Collaborator): Promise<Organization> {
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
    
        try {
            await queryRunner.query(`SET SCHEMA 'public'`);
        
            const organizationCreated = queryRunner.manager.create(Organization, organization);
            const organizationSaved = await queryRunner.manager.save(organizationCreated);
        
            const nameSchema = this.generateNameSchema(organizationSaved.name);
        
            const fullScriptTenant = await this.buildScriptTenant();
        
            await queryRunner.query(`
            CREATE SCHEMA IF NOT EXISTS "${nameSchema}"; 
            SET SCHEMA '${nameSchema}';
            ${fullScriptTenant}
            `);
            
            const password = crypto.randomBytes(10).toString('hex');
            collaborator.user.organizationId = organizationSaved.id;
            collaborator.user.name = await this.verifyExistsUserWithSameName(collaborator.person.name, collaborator.user.email);
            collaborator.user.password = await hash(password, 8);
            collaborator.user.isClient = false;
            collaborator.user.isCollaborator = true;
            collaborator.user.isManager = true;
            collaborator.user.emailChecked = false;
            collaborator.manager = true;

            const collaboratorCreated = queryRunner.manager.create(Collaborator, collaborator);
            await queryRunner.manager.save(collaboratorCreated);
        
            await queryRunner.query(`SET SCHEMA 'public'`);
            const tenant = queryRunner.manager.create(Tenant, { name: nameSchema, organizationId: organizationSaved.id });
            await queryRunner.manager.save(tenant);

            await NodemailerProvider.registerMail(collaborator.user.email.toLowerCase(), collaborator.person.name, collaborator.user.name, password);
        
            await queryRunner.commitTransaction();
            return organizationSaved;

        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new AppError({ message: 'Erro ao tentar criar tenant da empresa.' + error });

        } finally {
            await queryRunner.release();
        }
    }

    private generateNameSchema(organizationName: string) {
        let nameSchema = organizationName;
    
        nameSchema = nameSchema.replace(/[^\w\s]/gi, '').toLowerCase();
    
        nameSchema = nameSchema.replace(' ', '');
    
        if (nameSchema.length >= 5) {
          nameSchema = nameSchema.slice(0, 4);
        }
    
        nameSchema = `${nameSchema}_${crypto.createHmac('sha256', organizationName + new Date().getTime())
          .digest('hex').substr(0, 30)}`;
    
        return 'gb_' + nameSchema;
    }

    private async buildScriptTenant(): Promise<string> {
        const scriptsNames = [
          'tenant.sql',
        ];
    
        let fullScript: string = '';
    
        scriptsNames.forEach(scriptName => {
          const script = this.getScript(scriptName);
          fullScript += '\n' + script;
        });
    
        return fullScript;
      }

      private getScript(scriptFileName: string): string {
        const localScript = path.resolve("C:\\Users\\Tanya\\Documents\\Wallace\\TCC\\pg-2021-wallace-kruger-jr\\code\\Back", scriptFileName);

        let script = fs.readFileSync(localScript).toString();
    
        return script;
    }
    
    private async verifyExistsUserWithSameName(name: string, email: string){
      const connection = getConnection();
      const queryRunner = connection.createQueryRunner();
      await queryRunner.query(`SET SCHEMA 'public'`);

      let userNameArray = name.split(' ');
      let userName = `${userNameArray[0].toLowerCase()}.${userNameArray[1].toLowerCase()}`;
      let user = await queryRunner.manager.findOne(User, { name: userName });
      if(!user){
        return userName;
      }

      userNameArray = email.split('@');
      userName = userNameArray[0].toLowerCase();
      user = await queryRunner.manager.findOne(User, { name: userName });
      if(!user){
        return userName;
      }

      return email.toLowerCase();
    }

}

export default OrganizationRepository