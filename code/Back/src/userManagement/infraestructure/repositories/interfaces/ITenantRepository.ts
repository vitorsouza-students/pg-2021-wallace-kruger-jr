import Tenant from "../../../domain/Tenant";

export default interface ITenantRepository {

    findByOrganization(organizationId: number): Promise<Tenant | undefined>;
}