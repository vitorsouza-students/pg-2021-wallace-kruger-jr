import Collaborator from '../../../../projectManagement/domain/Collaborator';
import Organization from '../../../domain/Organization';

export default interface IOrganizationRepository {

    findByName(organizationName: string): Promise<Organization | undefined>;
    
    findByCnpj(cnpj: string): Promise<Organization | undefined>;
    
    create(organization: Organization, collaborator: Collaborator): Promise<Organization>;
}