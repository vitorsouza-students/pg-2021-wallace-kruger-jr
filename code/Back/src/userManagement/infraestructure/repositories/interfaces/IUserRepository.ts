import User from "../../../domain/User";

export default interface IUserRepository {

    findById(id: number): Promise<User | undefined>;

    findByName(name: string): Promise<User | undefined>;

    findByEmail(email: string): Promise<User | undefined>;

    update(user: User): Promise<User>;
}