import { Router } from 'express'
import { OrganizationController } from '../../interfaces/controllers/OrganizationController';

export class OrganizationRoute {

  public organizationRouter: Router;
  public organizationController: OrganizationController;

  constructor() {
    this.organizationRouter = Router();
    this.organizationController = new OrganizationController();
  }

  public routes() {
    this.organizationRouter.post('/', this.organizationController.create);

    return this.organizationRouter;
  }
}

export default new OrganizationRoute().routes();