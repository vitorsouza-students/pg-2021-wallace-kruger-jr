import { Router } from 'express'
import NodemailerProvider from '../../../shared/providers/Mail/NodemailerProvider';
import { ensureTokenEmail } from '../../../shared/interfaces/middlewares/ensureTokenEmail';
import { UserController } from '../../interfaces/controllers/UserController';
import { ensureAuthenticated } from '../../../shared/interfaces/middlewares/ensureAuthenticated';

export class UserRoute {

  public userRouter: Router;
  public userController: UserController;

  constructor() {
    this.userRouter = Router();
    this.userController = new UserController();
  }

  public routes() {
    this.userRouter.post('/login', this.userController.login);
    this.userRouter.post('/forgotpassword', this.userController.forgotPassword);
    this.userRouter.put('/resetpassword', ensureTokenEmail, this.userController.resetPassword);
    this.userRouter.put('/changepassword', ensureAuthenticated, this.userController.changePassword);

    return this.userRouter;
  }
}

export default new UserRoute().routes();