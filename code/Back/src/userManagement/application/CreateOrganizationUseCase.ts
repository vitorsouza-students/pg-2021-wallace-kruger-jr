import Organization from "../../userManagement/domain/Organization";
import IOrganizationRepository from "../infraestructure/repositories/interfaces/IOrganizationRepository";
import AppError from "../../shared/errors/AppError";
import validator from "../../shared/util/Validators";
import Collaborator from "../../projectManagement/domain/Collaborator";

class CreateOrganizationUseCase {

    constructor(
        private organizationRepository: IOrganizationRepository
    ) { }

    public async execute(organization: Organization, collaborator: Collaborator): Promise<Organization> {

        organization.cnpj = organization.cnpj.replace(/[\.\-\/]/g, '');
        this.verifyCnpjValid(organization.cnpj);
        await this.verifyExistsOrganizationWithSameCnpj(organization.cnpj);
        await this.verifyExistsOrganizationWithSameName(organization.name);

        collaborator.collaboratorData.cpf = collaborator.collaboratorData.cpf.replace(/[\.\-\/]/g, '');
        this.verifyCpfValid(collaborator.collaboratorData.cpf);

        const organizationCreated = await this.organizationRepository.create(organization, collaborator);

        return organizationCreated;
    }

    private verifyCnpjValid(cnpj: string) {
        const validateOrganizationDoc = validator.validateCnpj(cnpj);

        if (!validateOrganizationDoc.valid) {
            throw new AppError({ message: validateOrganizationDoc.response });
        }
    }

    private async verifyExistsOrganizationWithSameCnpj(cnpj: string) {
        const organization = await this.organizationRepository.findByCnpj(cnpj);

        if (organization) {
            throw new AppError({ message: 'Este CNPJ já está sendo usado por uma empresa.' });
        }
    }

    private async verifyExistsOrganizationWithSameName(organizationName: string) {
        const organization = await this.organizationRepository.findByName(organizationName);

        if (organization) {
            throw new AppError({ message: 'Já existe uma empresa cadastrada com esse nome.' });
        }
    }

    private verifyCpfValid(cpf: string) {
        const validateCollaboratorDoc = validator.validateCpf(cpf);

        if (!validateCollaboratorDoc.valid) {
            throw new AppError({ message: validateCollaboratorDoc.response });
        }
    }

}

export default CreateOrganizationUseCase;