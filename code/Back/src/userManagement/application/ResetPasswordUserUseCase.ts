import { hash } from "bcryptjs";
import AppError from "../../shared/errors/AppError";
import NodemailerProvider from "../../shared/providers/Mail/NodemailerProvider";
import IUserRepository from "../infraestructure/repositories/interfaces/IUserRepository";

class ResetPasswordUserUseCase {

    constructor(
        private userRepository: IUserRepository,
    ) { }

    public async execute(id: number, newPassword: string) {
        let user = await this.userRepository.findById(id);

        if(!user){
            throw new AppError({message: "Usuário não encontrado!", statusCode: 401, title: "Error! Não foi possível redefinir a senha."});
        }

        user.password = await hash(newPassword, 8);

        await this.userRepository.update({ ... user});

        NodemailerProvider.changePasswordMail(user.email);
    }
}

export default ResetPasswordUserUseCase;