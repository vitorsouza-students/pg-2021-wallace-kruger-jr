import { hash } from "bcryptjs";
import AppError from "../../shared/errors/AppError";
import { compare } from "bcryptjs";
import NodemailerProvider from "../../shared/providers/Mail/NodemailerProvider";
import IUserRepository from "../infraestructure/repositories/interfaces/IUserRepository";

class ChangePasswordUserUseCase {

    constructor(
        private userRepository: IUserRepository,
    ) { }

    public async execute(id: number, password: string, newPassword: string) {
        let user = await this.userRepository.findById(id);

        if(!user){
            throw new AppError({message: "Usuário não encontrado!", statusCode: 401, title: "Error! Não foi possível trocar a senha."});
        }

        const passwordMatch = await compare(password, user.password);

        if(!passwordMatch){
            throw new AppError({message: "A senha atual está incorreta!", statusCode: 401, title: "Error! Não foi possível trocar a senha."});
        }

        user.password = await hash(newPassword, 8);

        await this.userRepository.update({ ... user});

        NodemailerProvider.changePasswordMail(user.email);
    }
}

export default ChangePasswordUserUseCase;