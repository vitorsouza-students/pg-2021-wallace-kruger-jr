import AppError from "../../shared/errors/AppError";
import { compare } from "bcryptjs";
import { sign } from "jsonwebtoken";
import IUserRepository from "../infraestructure/repositories/interfaces/IUserRepository";
import ITenantRepository from "../infraestructure/repositories/interfaces/ITenantRepository";

class AuthenticateUserUseCase {

    constructor(
        private userRepository: IUserRepository,
        private tenantRepository: ITenantRepository
    ) { }

    public async execute(name: string, password: string): Promise<string> {
        
        const user = await this.userRepository.findByName(name);

        if(!user){
            throw new AppError({message: "Usuário ou senha incorretos!", statusCode: 401, title: "Error! Não foi possível acessar o sistema."});
        }

        const passwordMatch = await compare(password, user.password);

        if(!passwordMatch){
            throw new AppError({message: "Usuário ou senha incorretos!", statusCode: 401, title: "Error! Não foi possível acessar o sistema."});
        }

        const tenant = await this.tenantRepository.findByOrganization(user.organizationId);

        const token = sign({
            userId: user.id,
            email: user.email,
            client: user.isClient,
            collaborator: user.isCollaborator,
            manager: user.isManager,
            organizationId: user.organizationId,
            tenant: tenant.name,
            checked: user.emailChecked
        }, "29fef5a6-edb5-4309-b088-a7f3b134b3dc", {
            subject: user.id.toString(),
            expiresIn: "1h"
        });

        if(!user.emailChecked){
            let userUpdate = { ... user };
            userUpdate.emailChecked = true;
            this.userRepository.update(userUpdate);
        }

        return token;
    }
}

export default AuthenticateUserUseCase;