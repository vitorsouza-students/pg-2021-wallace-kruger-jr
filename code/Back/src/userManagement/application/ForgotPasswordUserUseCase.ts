import { hash } from "bcryptjs";
import AppError from "../../shared/errors/AppError";
import { sign } from "jsonwebtoken";
import NodemailerProvider from "../../shared/providers/Mail/NodemailerProvider";
import IUserRepository from "../infraestructure/repositories/interfaces/IUserRepository";

class ForgotPasswordUserUseCase {

    constructor(
        private userRepository: IUserRepository,
    ) { }

    public async execute(email: string) {
        let user = await this.userRepository.findByEmail(email);

        if(!user){
            //Não existe usuário cadastrado no sistema com este e-mail
            // throw new AppError({message: "Não existe usuário cadastrado no nosso sistema com este e-mail! Verifique se o e-mail foi digitado corretamente.", statusCode: 401, title: "Error! Não foi redefinir a senha."});
            return;
        }

        const token = sign({
            userId: user.id,
            email: user.email,
            name: user.name
        }, "3cb16c701e96416d1ffe", {
            subject: user.id.toString(),
            expiresIn: "1h"
        });

        NodemailerProvider.resetPasswordMail(email, token);
    }
}

export default ForgotPasswordUserUseCase;