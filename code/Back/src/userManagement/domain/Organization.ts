import { Column, Entity, Generated, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Tenant from "./Tenant";
import User from "./User";

@Entity('public.organization')
class Organization {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Generated('increment')
  code: string;

  @Column('varchar')
  name: string;

  @Column('varchar')
  cnpj: string;

  @Column('varchar')
  telephone: string;

  @Column('varchar')
  email: string;

  @OneToOne(type => Tenant, organization => Organization)
  tenant: Tenant;

  @OneToMany(type => User, user => User)
  users: User[];
}

export default Organization;