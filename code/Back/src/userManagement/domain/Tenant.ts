import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Organization from "./Organization";

@Entity('public.tenant')
class Tenant {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  name: string;

  @Column('int4', { name: "organization_id" })
  organizationId: number;

  @OneToOne(type => Organization, tenant => Tenant, { eager: true })
  @JoinColumn({ name: "organization_id" })
  organization: Organization;
}

export default Tenant;