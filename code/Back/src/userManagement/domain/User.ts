import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Collaborator from "../../projectManagement/domain/Collaborator";
import Client from "../../salesManagement/domain/Client";
import Organization from "./Organization";

@Entity('public.user')
class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Generated('increment')
  code: string;

  @Column('varchar')
  name: string;

  @Column('varchar')
  email: string;

  @Column('varchar')
  password: string;

  @Column('boolean', { name: "is_client" })
  isClient: boolean;

  @Column('boolean', { name: "is_collaborator" })
  isCollaborator: boolean;

  @Column('boolean', { name: "is_manager" })
  isManager: boolean;

  @Column('boolean', { name: "email_checked" })
  emailChecked: boolean;

  @Column('int4', { name: "organization_id" })
  organizationId: number;

  @ManyToOne(type => Organization, organization => organization.users, { eager: true })
  @JoinColumn({ name: "organization_id" })
  organization: Organization;

  @OneToOne(type => Client, user => User)
  client: Client;

  @OneToOne(type => Collaborator, user => User)
  collaborator: Collaborator;
}

export default User;