import { Router } from 'express'
import { ZipCodeController } from '../../interfaces/controllers/ZipCodeController';

export class ZipCodeRoute {

  public zipCodeRouter: Router;
  public zipCodeController: ZipCodeController;

  constructor() {
    this.zipCodeRouter = Router();
    this.zipCodeController = new ZipCodeController();
  }

  public routes() {
    this.zipCodeRouter.get('/:zipcode', this.zipCodeController.find);
    return this.zipCodeRouter;
  }
}

export default new ZipCodeRoute().routes();