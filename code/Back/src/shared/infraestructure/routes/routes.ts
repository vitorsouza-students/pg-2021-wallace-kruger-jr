import { Router } from "express";

//UserManagement
import organizationRoute from '../../../userManagement/infraestructure/routes/organization.routes';
import userRoute from '../../../userManagement/infraestructure/routes/user.routes';

//SalesManagement
import funnelSlotRoute from '../../../salesManagement/infraestructure/routes/funnelSlot.routes';
import leadRoute from '../../../salesManagement/infraestructure/routes/leads.routes';
import clientRoute from '../../../salesManagement/infraestructure/routes/client.routes';
import proposalRoute from '../../../salesManagement/infraestructure/routes/proposal.routes';
import contractRoute from '../../../salesManagement/infraestructure/routes/contract.routes';
import contractStatusRoute from '../../../salesManagement/infraestructure/routes/contractStatus.routes';
import cityRoute from '../../../salesManagement/infraestructure/routes/city.routes';

//ProjectManagement
import projectRoute from '../../../projectManagement/infraestructure/routes/project.routes';

//Shared
import zipcodesRoute from "./zipCode.routes";
import { ensureAuthenticated } from "../../interfaces/middlewares/ensureAuthenticated";

const routes = Router();

routes.use('/organizations', organizationRoute);
routes.use('/users', userRoute);

routes.use('/funnelslots', ensureAuthenticated, funnelSlotRoute);
routes.use('/leads', ensureAuthenticated, leadRoute);
routes.use('/clients', ensureAuthenticated, clientRoute);
routes.use('/proposals', ensureAuthenticated, proposalRoute);
routes.use('/contracts', ensureAuthenticated, contractRoute);
routes.use('/contractsstatus', contractStatusRoute);
routes.use('/citys', cityRoute);

routes.use('/projects', ensureAuthenticated, projectRoute);

routes.use('/zipcodes', zipcodesRoute);

export { routes };