import fs from "fs";
import path from "path";
import Mustache from "mustache";

const nodemailer = require('nodemailer');

const SMTP_CONFIG = require('./config/smtp');

const transporter = nodemailer.createTransport({
    host: SMTP_CONFIG.host,
    port: SMTP_CONFIG.port,
    secure: false,
    auth: {
        user: SMTP_CONFIG.user,
        pass: SMTP_CONFIG.pass
    },
    tls: {
        rejectUnauthorized: false
    }
});

class NodemailerProvider {
    
    public async registerMail(addressee: string, name: string, username: string, password: string) {
        const variables = {
            "name": name,
            "username": username,
            "password": password,
        }
        const localScript = path.resolve(SMTP_CONFIG.templates, "registerMail.html");
        
        await transporter.sendMail({
            to: [addressee],
            from: "ganttbox@gmail.com",
            subject: "Bem vindo ao GanttBox!",
            text: "Seu cadastro foi realizado com sucesso! Seja bem vindo ao sistema de gestão GanttBox.",
            html: Mustache.render(fs.readFileSync(localScript, 'utf-8'), variables)
        });
    }

    public async changePasswordMail(addressee: string) {
        const localScript = path.resolve(SMTP_CONFIG.templates, "changePasswordEmail.html");
        
        await transporter.sendMail({
            to: [addressee],
            from: "ganttbox@gmail.com",
            subject: "Troca de Senha",
            text: "Conforme solicitado, sua senha foi alterada com sucesso!",
            html: fs.readFileSync(localScript, 'utf-8')
        });
    }

    public async resetPasswordMail(addressee: string, token: string) {
        const variables = {
            "urlFrontEnd": "http://localhost:4200",
            "tokenEmail": token,
        }
        const localScript = path.resolve(SMTP_CONFIG.templates, "resetPasswordEmail.html");
        
        await transporter.sendMail({
            to: [addressee],
            from: "ganttbox@gmail.com",
            subject: "Esqueceu a Senha?",
            text: `Conforme solicitado, sua senha pode ser alterada pelo link \'http://localhost:4200/redefinir-senha?token=${token}\'`,
            html: Mustache.render(fs.readFileSync(localScript, 'utf-8'), variables)
        });
    }
}

export default new NodemailerProvider();
