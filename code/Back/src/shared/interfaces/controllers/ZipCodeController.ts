import { Request, Response } from 'express';
import FindAddressByZipCodeUseCase from '../../application/FindAddressByZipCodeUseCase';

export class ZipCodeController {

  public async find(request: Request, response: Response) {
    const tenant = request.headers.tenant ? <string>request.headers.tenant : '';

    try {
        const { zipcode } = request.params;

        const findAddressByZipCodeUseCase = new FindAddressByZipCodeUseCase();
        const address = await findAddressByZipCodeUseCase.execute(zipcode);

        return response.status(200).json(address);

    } catch (err) {
        return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
    }
  }

}