import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";


export function ensureTokenEmail(request: Request, response: Response, next: NextFunction){
    const token = request.headers.tokenemail;

    if(!token) {
        return response.status(401).json({message: "O usuário não esta autenticado."});
    }

    try {
        verify(token.toString(), "3cb16c701e96416d1ffe");
        return next();        
    } catch (error) {
        return response.status(401).json({message: "O usuário não esta autenticado."});
    }
}