import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";


export function ensureAuthenticated(request: Request, response: Response, next: NextFunction){
    const token = request.headers.token;
    
    if(!token) {
        return response.status(401).json({message: "O usuário não esta autenticado."});
    }

    try {
        verify(token.toString(), "29fef5a6-edb5-4309-b088-a7f3b134b3dc");
        return next();        
    } catch (error) {
        return response.status(401).json({message: "O usuário não esta autenticado."});
    }
}