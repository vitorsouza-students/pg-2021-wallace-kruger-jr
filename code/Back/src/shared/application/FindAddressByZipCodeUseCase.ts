import AppError from "../errors/AppError";

const axios = require('axios');

class FindAddressByZipCodeUseCase {

    constructor(
    ) { }

    public async execute(zipCode: string) {
        try {            
            const response = await axios.get(`https://viacep.com.br/ws/${zipCode}/json/`);
            return response.data;
            
        } catch (error) {
            throw new AppError({message: "Error ao buscar CEP!"});
        }
    }
}

export default FindAddressByZipCodeUseCase;