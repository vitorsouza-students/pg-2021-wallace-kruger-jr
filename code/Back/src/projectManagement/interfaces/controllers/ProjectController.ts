import { Request, Response } from 'express';
import FindAllProjectsPagedUseCase from '../../application/Project/FindAllProjectsPagedUseCase';
import ProjectRepository from '../../infraestructure/repositories/ProjectRepository';

export class ProjectController {

    public async findAll(request: Request, response: Response) {
        const tenant = request.headers.tenant ? <string>request.headers.tenant : '';
        const projectRepository = new ProjectRepository(tenant);
    
        try {
            const { page, limit } = request.query;
            const getPage = page || 1;
            const getLimit = limit || 10;
    
            const findAllProjectsPagedUseCase = new FindAllProjectsPagedUseCase(projectRepository);
            const projects = await findAllProjectsPagedUseCase.execute({
                page: +getPage,
                limit: +getLimit
            });
    
            return response.status(200).json(projects);
    
        } catch (err) {
            return response.status(err.statusCode || 500).json({ message: err.message, title: err.title });
        }
      }

}