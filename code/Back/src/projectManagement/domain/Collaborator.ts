import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Person from "../../salesManagement/domain/Person";
import User from "../../userManagement/domain/User";
import BankData from "./BankData";
import CollaboratorData from "./CollaboratorData";
import Sector from "./Sector";

@Entity('collaborator')
class Collaborator {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Generated('increment')
  code: string;

  @Column('boolean')
  manager: boolean;

  @Column('int4', { name: "user_id" })
  userId: number;

  @OneToOne(type => User, collaborator => Collaborator, { eager: true, cascade: true })
  @JoinColumn({ name: "user_id" })
  user: User;

  @Column('int4', { name: "person_id" })
  personId: number;

  @OneToOne(type => Person, collaborator => Collaborator, { eager: true, cascade: true })
  @JoinColumn({ name: "person_id" })
  person: Person;

  @Column('int4', { name: "collaborator_data_id" })
  collaboratorDataId: number;

  @OneToOne(type => CollaboratorData, collaborator => Collaborator, { eager: true, cascade: true })
  @JoinColumn({ name: "collaborator_data_id" })
  collaboratorData: CollaboratorData;

  @Column('int4', { name: "bank_data_id" })
  bankDataId: number;

  @OneToOne(type => BankData, collaborator => Collaborator, { eager: true, cascade: true })
  @JoinColumn({ name: "bank_data_id" })
  bankData: BankData;

  @Column('int4', { name: "sector_id" })
  sectorId: number;

  @ManyToOne(type => Sector, sector => sector.collaborators, { eager: true })
  @JoinColumn({ name: "sector_id" })
  sector: Sector;
}

export default Collaborator;