import { Column, Entity, Generated, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('project')
class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Generated('increment')
  code: string;

  @Column('varchar')
  name: string;
}

export default Project;