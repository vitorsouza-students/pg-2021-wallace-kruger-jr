import { Column, Entity, Generated, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import Collaborator from "./Collaborator";

@Entity('sector')
class Sector {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Generated('increment')
  code: string;

  @Column('varchar')
  name: string;

  @OneToMany(type => Collaborator, collaborator => Collaborator)
  collaborators: Collaborator[];
}

export default Sector;