import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Collaborator from "./Collaborator";

@Entity('bank_data')
class BankData {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  bank: string;

  @Column('varchar')
  agency: string;

  @Column('varchar')
  account: string;

  @OneToOne(type => Collaborator, bankData => BankData)
  collaborator: Collaborator;
}

export default BankData;