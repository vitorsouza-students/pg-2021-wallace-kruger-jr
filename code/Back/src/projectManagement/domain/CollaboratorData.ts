import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import Collaborator from "./Collaborator";

@Entity('collaborator_data')
class CollaboratorData {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  rg: string;

  @Column('varchar')
  cpf: string;

  @Column('varchar', { name: 'pis_pasep'})
  pisPasep: string;

  @Column('varchar')
  schooling: string;

  @Column('varchar', { name: 'number_of_dependents'})
  numberOfDependents: number;

  @Column('varchar', { name: 'civil_status' })
  civilStatus: string;

  @Column('varchar')
  gender: string;

  @OneToOne(type => Collaborator, collaboratorData => CollaboratorData)
  collaborator: Collaborator;
}

export default CollaboratorData;