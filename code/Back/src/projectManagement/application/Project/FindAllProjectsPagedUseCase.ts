import Project from "../../domain/Project";
import IProjectRepository from "../../infraestructure/repositories/interfaces/IProjectRepository";

interface Props {
    page: number,
    limit: number
  }

class FindAllProjectsPagedUseCase {

    constructor(
        private projectRepository: IProjectRepository,
    ) { }

    public async execute({ page, limit }: Props): Promise<[Project[] | undefined, number]> {
        const projects = await this.projectRepository.findAllProjectsPaged(page, limit);
        
        return projects;
    }
}

export default FindAllProjectsPagedUseCase;