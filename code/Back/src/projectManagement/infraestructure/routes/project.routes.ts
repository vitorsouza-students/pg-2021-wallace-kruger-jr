import { Router } from 'express'
import { ProjectController } from '../../interfaces/controllers/ProjectController';

export class ProjectRoute {

  public projectRouter: Router;
  public projectController: ProjectController;

  constructor() {
    this.projectRouter = Router();
    this.projectController = new ProjectController();
  }

  public routes() {
    this.projectRouter.get('', this.projectController.findAll);

    return this.projectRouter;
  }
}

export default new ProjectRoute().routes();