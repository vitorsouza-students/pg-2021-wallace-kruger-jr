import { getConnection, getRepository, IsNull, Repository } from "typeorm";
import Project from '../../domain/Project';
import IProjectRepository from "./interfaces/IProjectRepository";


class ProjectRepository implements IProjectRepository {

    private ormRepository: Repository<Project>;
  
    constructor(private tenant: string) {
        this.ormRepository = getRepository(Project);
    }
    
    public async findAllProjectsPaged(page: number, limit: number): Promise<[Project[] | undefined, number]> {
        await this.ormRepository.query(`SET SCHEMA '${this.tenant}'`);

        const offset = (page - 1) * limit;
        
        const [ projects, count] = await this.ormRepository.createQueryBuilder('project')
            .limit(limit)
            .offset(offset)
            .getManyAndCount();

        return [ projects, count];
    }

}

export default ProjectRepository