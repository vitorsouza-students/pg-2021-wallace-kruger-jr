import Project from '../../../domain/Project';

export default interface IProjectRepository {

    findAllProjectsPaged(page: number, limit: number): Promise<[Project[] | undefined, number]>;
}