import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Login } from '../models/Login';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urlBase = `${environment.URL_GB_BACK}/users`;

  constructor(
    private http: HttpClient,
  ) { }

  public login(login: Login) : Promise<string | undefined>{
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.post<string>(`${this.urlBase}/login`, login, {headers}).toPromise();
  }

  public forgotPassword(forgotPassword: any) {
    return this.http.post(`${this.urlBase}/forgotpassword`, forgotPassword).toPromise();
  }

  public resetPassword(resetPassword: any, token: string) {
    const headers = new HttpHeaders({ 'tokenemail': token });
    return this.http.put(`${this.urlBase}/resetpassword`, resetPassword, {headers}).toPromise();
  }

  public changepassword(changePassword: any) {
    return this.http.put(`${this.urlBase}/changepassword`, changePassword).toPromise();
  }
}
