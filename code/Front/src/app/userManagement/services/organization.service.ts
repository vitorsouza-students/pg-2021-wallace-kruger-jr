import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Collaborator } from '../models/Collaborator';
import { Organization } from '../models/Organization';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  private urlBase = `${environment.URL_GB_BACK}/organizations`;

  constructor(
    private http: HttpClient,
  ) { }

  public register(organization: Organization, collaborator: Collaborator) : Promise<Organization | undefined>{
    return this.http.post<Organization>(`${this.urlBase}/`, [organization, collaborator]).toPromise();
  }
}
