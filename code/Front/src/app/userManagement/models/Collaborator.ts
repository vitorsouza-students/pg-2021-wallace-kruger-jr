import { CollaboratorData } from './CollaboratorData';
import { Person } from './Person';
import User from './User';

export class Collaborator {
  id?: number;
  code?: string;
  manager?: boolean;
  userId?: number;
  user: User;
  personId?: number;
  person: Person;
  collaboratorDataId?: number;
  collaboratorData: CollaboratorData;

  constructor() {
    this.user = new User();
    this.person = new Person();
    this.collaboratorData = new CollaboratorData()
  }
}
