export class Organization {
  id?: number;
  code?: string;
  name?: string;
  cnpj?: string;
  telephone?: string;
  email?: string;
}
