import { Contact } from "./Contact";

export class Person {
  id?: number;
  name?: string;
  cpf?: string;
  contactId?: number;
  contact: Contact;

  constructor(){
    this.contact = new Contact();
  }
}
