export class CollaboratorData {
  id?: number;
  rg?: string;
  cpf?: string;
  pisPasep?: string;
  schooling?: string;
  numberOfDependents?: number;
  civilStatus?: string;
  gender?: string;
}
