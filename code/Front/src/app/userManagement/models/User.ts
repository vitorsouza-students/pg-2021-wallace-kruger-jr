export class User {
  id?: number;
  code?: string;
  name?: string;
  email?: string;
  password?: string;
  emailChecked?: boolean;
  organizationId?: number;
}

export default User;
