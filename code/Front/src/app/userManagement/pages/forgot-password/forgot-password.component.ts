import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../services/user.service';
import { ModalComponent } from '../../../shared/components/modal/modal.component';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  formGroup!: FormGroup;
  modalOptions!: NgbModalOptions;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private modalService: NgbModal
    ) { }

  ngOnInit(): void {
    this.buildForm();
    this.configModal();
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email] ],
    });
  }

  configModal(){
    this.modalOptions = {
      centered: true,
      size: "md"
    }
  }

  inputError(formName: FormGroup, propertyName: string, validatorName: string){
    return formName?.controls[propertyName]?.errors?.[validatorName] && formName?.controls[propertyName]?.['touched'];
  }

  handleSubmit(){
    this.formGroup.markAllAsTouched();
    if (!this.formGroup.valid) {
      return;
    }

    const forgotPassword = {
      email: this.formGroup.value.email
    }

    this.submit(forgotPassword);
  }

  async submit(forgotPassword: any){
    await this.userService.forgotPassword(forgotPassword);
    const modalRef = this.modalService.open(ModalComponent, this.modalOptions);
    modalRef.componentInstance.title = 'E-mail enviado!';
    modalRef.componentInstance.content = 'Verifique sua caixa de entrada. Se o e-mail informado esta associado a algum usuário em nossa base de dados, você receberá um e-mail para poder redefinir sua senha e voltar ter acesso ao sistema!';
    this.activeModal.close();
  }

}
