import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import ValidatesFormGroup from 'src/app/shared/utils/ValidatesFormGroup';
import jwt_decode from "jwt-decode";
import { UserService } from '../../services/user.service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { ToastService } from 'src/app/shared/services/toats.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  formGroup!: FormGroup;
  token: any;
  profile: any;
  modalOptions!: NgbModalOptions;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private modalService: NgbModal,
    private toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.decodeTokenEmail();
    this.buildForm();
    this.configModal();
  }

  decodeTokenEmail(){
    this.token = this.route.snapshot.queryParamMap.get("token");
    this.profile = jwt_decode(this.token.toString());
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      password: ['', Validators.required ],
      confirmPassword: ['', Validators.required ],
    }, {
      validators: [ValidatesFormGroup.passwordsMatch]
    });
  }

  configModal(){
    this.modalOptions = {
      centered: true,
      size: "sm"
    }
  }

  inputError(formName: FormGroup, propertyName: string, validatorName: string){
    return formName?.controls[propertyName]?.errors?.[validatorName] && formName?.controls[propertyName]?.['touched'];
  }

  handleRegister(){
    this.formGroup.markAllAsTouched();
    if (!this.formGroup.valid) {
      return;
    }

    const resetPassword = this.getValuesForm();

    this.changePassword(resetPassword);
  }

  getValuesForm() {
    const formGroup = this.formGroup.value;
    const resetPassword = {
      id: this.profile.userId,
      newPassword: formGroup.password
    }

    return resetPassword;
  }

  async changePassword(resetPassword: any){
    try {
      await this.userService.resetPassword(resetPassword, this.token.toString());

      const modalRef = this.modalService.open(ModalComponent, this.modalOptions);
      modalRef.componentInstance.title = 'Senha alterada!';
      modalRef.componentInstance.content = 'Senha alterada com sucesso! Agora você poderá acessar o sistema novamente com sua nova senha.';

      this.router.navigateByUrl("login");
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

}
