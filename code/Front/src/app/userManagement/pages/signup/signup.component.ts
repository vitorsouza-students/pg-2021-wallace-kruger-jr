import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { ToastService } from 'src/app/shared/services/toats.service';
import ValidatesFormGroup from 'src/app/shared/utils/ValidatesFormGroup';
import { Collaborator } from '../../models/Collaborator';
import { Organization } from '../../models/Organization';
import { OrganizationService } from '../../services/organization.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  organization: Organization = new Organization();
  collaborator: Collaborator = new Collaborator();
  organizationForm!: FormGroup;
  collaboratorForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private organizationService: OrganizationService,
    private toastService: ToastService,
    private modalService: NgbModal,
    ){}

  ngOnInit(){
    this.buildOrganizationForm();
    this.buildCollaboratorForm();
  }

  buildOrganizationForm() {
    this.organizationForm = this.formBuilder.group({
      name: ['', Validators.required ],
      cnpj: ['', Validators.required ],
      telephone: ['', Validators.required ],
      email: ['', [Validators.required, Validators.email] ],
    }, {
      validators: [ValidatesFormGroup.validatorCnpj]
    });
  }

  buildCollaboratorForm(){
    this.collaboratorForm = this.formBuilder.group({
      name: ['', Validators.required ],
      email: ['', [Validators.required, Validators.email] ],
      rg: ['', Validators.required ],
      cpf: ['', Validators.required ],
      cellphone: ['', Validators.required ],
    }, {
      validators: [ValidatesFormGroup.validatorCpf, ValidatesFormGroup.fullName]
    });
  }

  handleRegister(){
    this.organizationForm.markAllAsTouched();
    this.collaboratorForm.markAllAsTouched();
    if ((!this.organizationForm.valid) || (!this.collaboratorForm.valid)) {
      return;
    }

    this.getValuesForms();

    this.register();
  }

  getValuesForms() {
    const organizationForm = this.organizationForm.value;
    this.organization.name = organizationForm.name;
    this.organization.cnpj = organizationForm.cnpj;
    this.organization.telephone = organizationForm.telephone;
    this.organization.email = organizationForm.email;


    const collaboratorForm = this.collaboratorForm.value;
    this.collaborator.user.email = collaboratorForm.email;
    this.collaborator.person.name = collaboratorForm.name;
    this.collaborator.person.cpf = collaboratorForm.cpf;
    this.collaborator.person.contact.email = collaboratorForm.email;
    this.collaborator.person.contact.cellphone = collaboratorForm.cellphone;
    this.collaborator.collaboratorData.cpf = collaboratorForm.cpf;
    this.collaborator.collaboratorData.rg = collaboratorForm.rg;
  }

  async register(){
    try {
      const result = await this.organizationService.register(this.organization, this.collaborator);

      const modalRef = this.modalService.open(ModalComponent, { centered: true, size: "lg"});
      modalRef.componentInstance.title = 'Legal! Seu cadastro foi realizado com sucesso!';
      modalRef.componentInstance.content = 'Por favor, entre no seu e-mail para ter acessso a suas credencias do GanttBox.';

      this.router.navigateByUrl("login");
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  inputError(formName: FormGroup, propertyName: string, validatorName: string){
    return formName?.controls[propertyName]?.errors?.[validatorName] && formName?.controls[propertyName]?.['touched'];
  }

  handleNavigateToLogin(){
    this.router.navigateByUrl("login");
  }

}
