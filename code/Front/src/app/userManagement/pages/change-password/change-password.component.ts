import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Profile } from 'src/app/shared/models/Profile';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { ToastService } from 'src/app/shared/services/toats.service';
import ValidatesFormGroup from 'src/app/shared/utils/ValidatesFormGroup';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  formGroup!: FormGroup;
  profile!: Profile;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private modalService: NgbModal,
    private profileService: ProfileService,
    private toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.getProfile();
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      currentPassword: ['', Validators.required ],
      password: ['', Validators.required ],
      confirmPassword: ['', Validators.required ],
    }, {
      validators: [ValidatesFormGroup.passwordsMatch]
    });
  }

  getProfile() {
    this.profile = this.profileService.getProfile();
  }

  inputError(formName: FormGroup, propertyName: string, validatorName: string){
    return formName?.controls[propertyName]?.errors?.[validatorName] && formName?.controls[propertyName]?.['touched'];
  }

  handleSubmit(){
    this.formGroup.markAllAsTouched();
    if (!this.formGroup.valid) {
      return;
    }

    const changePassword = this.getValuesForm();

    this.changePassword(changePassword);
  }

  getValuesForm() {
    const formGroup = this.formGroup.value;

    const changePassword = {
      id: this.profile.userId,
      password: formGroup.currentPassword,
      newPassword: formGroup.password
    }

    return changePassword;
  }

  async changePassword(changePassword: any){
    try {
      await this.userService.changepassword(changePassword);
      this.toastService.show("Senha alterada com Sucesso!", { classname: 'bg-success text-light' });
      this.router.navigateByUrl("projetos");
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

}
