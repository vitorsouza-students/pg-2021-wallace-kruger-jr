import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Login } from '../../models/Login';
import { UserService } from '../../services/user.service';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { CookieService } from 'ngx-cookie-service';
import { ToastService } from '../../../shared/services/toats.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;
  login: Login = new Login();
  modalOptions!: NgbModalOptions;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private modalService: NgbModal,
    private cookieService: CookieService,
    private toastService: ToastService
    ){}

  ngOnInit(){
    this.buildForm();
    this.configModalForgotPassword();
  }

  buildForm() {
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required ],
      password: ['', Validators.required ]
    });
  }

  configModalForgotPassword(){
    this.modalOptions = {
      centered: true,
      size: "lg"
    }
  }

  handleSubmit(){
    this.loginForm.markAllAsTouched();
    if (!this.loginForm.valid) {
      return;
    }

    this.getValuesLoginForm();

    this.authenticated();
  }

  getValuesLoginForm() {
    const form = this.loginForm.value;

    this.login.name = form.name;
    this.login.password = form.password;
  }

  async authenticated(){
    try {
      const token : any = await this.userService.login(this.login);
      this.cookieService.deleteAll();
      this.cookieService.set('token', token.toString());
      this.router.navigateByUrl("projetos");
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  inputError(formName: FormGroup, propertyName: string, validatorName: string){
    return formName?.controls[propertyName]?.errors?.[validatorName] && formName?.controls[propertyName]?.['touched'];
  }

  handleNavigateToSignUp(){
    this.router.navigateByUrl("cadastrar");
  }

  handleOpenModal() {
    this.modalService.open(ForgotPasswordComponent, this.modalOptions);
  }

}
