import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './userManagement/pages/login/login.component';
import { SignupComponent } from './userManagement/pages/signup/signup.component';
import { ResetPasswordComponent } from './userManagement/pages/reset-password/reset-password.component';
import { PainelComponent } from './shared/components/painel/painel.component';
import { ProjectListComponent } from './projectManagement/pages/project-list/project-list.component';
import { ChangePasswordComponent } from './userManagement/pages/change-password/change-password.component';
import { AuthenticationGuard } from './shared/guards/authentication.guard';
import { SalesFunnelComponent } from './salesManagement/pages/sales-funnel/sales-funnel.component';
import { PainelSalesComponent } from './salesManagement/pages/painel-sales/painel-sales.component';
import { ClientListComponent } from './salesManagement/pages/client-list/client-list.component';
import { ProposalListComponent } from './salesManagement/pages/proposal-list/proposal-list.component';
import { ContractListComponent } from './salesManagement/pages/contract-list/contract-list.component';

const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "prefix" },
  { path: 'login', component: LoginComponent},
  { path: 'cadastrar', component: SignupComponent},
  { path: 'redefinir-senha', component: ResetPasswordComponent},
  {
    path: "",
    component: PainelComponent,
    children: [
      { path: 'trocar-senha', component: ChangePasswordComponent},
      { path: 'projetos', component: ProjectListComponent },
      {
        path: '',
        component: PainelSalesComponent, children: [
          { path: 'funil-vendas', component: SalesFunnelComponent },
          { path: 'clientes', component: ClientListComponent },
          { path: 'propostas', component: ProposalListComponent },
          { path: 'contratos', component: ContractListComponent }
        ] }
    ], canActivate: [AuthenticationGuard]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
