import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { Profile } from '../../../shared/models/Profile';
import { ConfirmModalComponent } from '../../../shared/components/confirm-modal/confirm-modal.component';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {

  profile!: Profile;

  constructor(
    private profileService: ProfileService,
    private modalService: NgbModal,
    private router: Router,
    private cookieService: CookieService,
    private projectService: ProjectService
    ) { }

  ngOnInit(): void {
    this.getProfile();
    this.checkFirstAccess();
    this.loadProjects();
  }

  getProfile() {
    this.profile = this.profileService.getProfile();
  }

  async checkFirstAccess(){
    if(!this.profile.checked && !this.cookieService.get('acess')) {
      this.cookieService.set('acess', "ok");
      const modalRef = this.modalService.open(ConfirmModalComponent, { centered: true, size: "lg"});
      modalRef.componentInstance.title = 'Seja muito bem vindo ao GanttBox!';
      modalRef.componentInstance.content = 'Para sua segurança, recomendamos a troca da sua senha.';
      modalRef.componentInstance.negationText = 'Depois';
      modalRef.componentInstance.confirmationText = 'Trocar agora';

      modalRef.result.then(result => {
        if(result){
          this.router.navigateByUrl("trocar-senha");
        }
      });
    }
  }

  async loadProjects(){
    const projects = await this.projectService.getAll();
  }


}
