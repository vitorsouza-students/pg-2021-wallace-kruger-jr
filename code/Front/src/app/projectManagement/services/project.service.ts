import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private urlBase = `${environment.URL_GB_BACK}/projects`;

  constructor(
    private http: HttpClient,
  ) { }

  public getAll() {
    return this.http.get(`${this.urlBase}/`).toPromise();
  }
}
