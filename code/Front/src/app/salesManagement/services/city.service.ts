import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { City } from '../models/City';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  private urlBase = `${environment.URL_GB_BACK}/citys`;

  constructor(
    private http: HttpClient,
  ) { }

  public getCitys(uf: string): Promise<City[] | undefined> {
    return this.http.get<City[]>(`${this.urlBase}/${uf}`).toPromise();
  }

  public getUfs() {
    return this.http.get(`${this.urlBase}/uf/all`).toPromise();
  }
}
