import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FunnelSlotDTO } from '../dtos/FunnelSlotDTO';

@Injectable({
  providedIn: 'root'
})
export class FunnelSlotService {

  private urlBase = `${environment.URL_GB_BACK}/funnelslots`;

  constructor(
    private http: HttpClient,
  ) { }

  public getSlots(subsystem: string): Promise<FunnelSlotDTO[] | undefined> {
    return this.http.get<FunnelSlotDTO[]>(`${this.urlBase}/${subsystem}`).toPromise();
  }
}
