import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LeadsInFunnelDTO } from '../dtos/LeadsInFunnelDTO';
import { CreateLeadDTO } from '../dtos/CreateLeadDTO';
import { Lead } from '../models/Lead';
import { DeclareWinLossLeadDTO } from '../dtos/DeclareWinLossLeadDTO';

@Injectable({
  providedIn: 'root'
})
export class LeadService {

  private urlBase = `${environment.URL_GB_BACK}/leads`;

  constructor(
    private http: HttpClient,
  ) { }

  public getLeads() : Promise<LeadsInFunnelDTO[] | undefined> {
    return this.http.get<LeadsInFunnelDTO[]>(`${this.urlBase}/`).toPromise();
  }

  public createLead(lead: CreateLeadDTO) : Promise<Lead | undefined> {
    return this.http.post<Lead>(`${this.urlBase}/`, lead).toPromise();
  }

  public updateLead(lead: CreateLeadDTO) : Promise<Lead | undefined> {
    return this.http.put<Lead>(`${this.urlBase}/`, lead).toPromise();
  }

  public changeStatusLead(lead: CreateLeadDTO) : Promise<Lead | undefined> {
    const params = { disableLoading: 's' }
    return this.http.put<Lead>(`${this.urlBase}/changestatus`, lead, { params: params }).toPromise();
  }

  public declareWinLoss(lead: DeclareWinLossLeadDTO) : Promise<Lead | undefined> {
    return this.http.put<Lead>(`${this.urlBase}/declarewinloss`, lead).toPromise();
  }

  public deleteLead(leadId: number | undefined) {
    return this.http.delete(`${this.urlBase}/${leadId}`).toPromise();
  }
}
