import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Client } from '../models/Client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private urlBase = `${environment.URL_GB_BACK}/clients`;

  constructor(
    private http: HttpClient,
  ) { }

  public getClients() {
    return this.http.get(`${this.urlBase}/`).toPromise();
  }

  public createLead(client: Client) : Promise<Client | undefined> {
    return this.http.post<Client>(`${this.urlBase}/`, client).toPromise();
  }
}
