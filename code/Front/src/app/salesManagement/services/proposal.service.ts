import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LeadsInFunnelDTO } from '../dtos/LeadsInFunnelDTO';
import { Proposal } from '../models/Proposal';

@Injectable({
  providedIn: 'root'
})
export class ProposalService {

  private urlBase = `${environment.URL_GB_BACK}/proposals`;

  constructor(
    private http: HttpClient,
  ) { }

  public getProposalByLead(leadId: number | undefined) : Promise<Proposal | undefined> {
    return this.http.get<Proposal>(`${this.urlBase}/${leadId}`).toPromise();
  }

  public getProposals() {
    return this.http.get(`${this.urlBase}/`).toPromise();
  }

  public createProposal(proposal: Proposal | undefined) : Promise<Proposal | undefined> {
    return this.http.post<Proposal>(`${this.urlBase}/`, proposal).toPromise();
  }

  public updateProposal(proposal: Proposal | undefined) : Promise<Proposal | undefined> {
    return this.http.put<Proposal>(`${this.urlBase}/`, proposal).toPromise();
  }

  public deleteProposal(proposalId: number | undefined) {
    return this.http.delete(`${this.urlBase}/${proposalId}`).toPromise();
  }
}
