import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Contract } from '../models/Contract';
import { ContractDTO } from '../dtos/ContractDTO';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  private urlBase = `${environment.URL_GB_BACK}/contracts`;

  constructor(
    private http: HttpClient,
  ) { }

  public getContractByLead(leadId: number | undefined) : Promise<Contract | undefined> {
    return this.http.get<Contract>(`${this.urlBase}/${leadId}`).toPromise();
  }

  public getContracts() {
    return this.http.get(`${this.urlBase}/`).toPromise();
  }

  public createContract(contract: ContractDTO | undefined) : Promise<Contract | undefined> {
    return this.http.post<Contract>(`${this.urlBase}/`, contract).toPromise();
  }

  public updateContract(contract: ContractDTO | undefined) : Promise<Contract | undefined> {
    return this.http.put<Contract>(`${this.urlBase}/`, contract).toPromise();
  }

  public deleteContract(contractId: number | undefined) {
    return this.http.delete(`${this.urlBase}/${contractId}`).toPromise();
  }
}
