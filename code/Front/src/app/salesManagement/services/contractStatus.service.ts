import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ContractStatus } from '../models/ContractStatus';

@Injectable({
  providedIn: 'root'
})
export class ContractStatusService {

  private urlBase = `${environment.URL_GB_BACK}/contractsstatus`;

  constructor(
    private http: HttpClient,
  ) { }

  public getAll() : Promise<ContractStatus[] | undefined> {
    return this.http.get<ContractStatus[]>(`${this.urlBase}/`).toPromise();
  }
}
