import { Component, OnInit, ViewChild } from '@angular/core';
import { CardSettingsModel, DataSourceChangedEventArgs, DialogEventArgs, KanbanComponent } from '@syncfusion/ej2-angular-kanban';
import { L10n } from "@syncfusion/ej2-base";
import { FunnelSlotService } from '../../services/funnelSlot.service';
import { LeadService } from '../../services/lead.service';
import { LeadsInFunnelDTO } from '../../dtos/LeadsInFunnelDTO';
import { FunnelSlotDTO } from '../../dtos/FunnelSlotDTO';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LeadFormModalComponent } from '../lead-form-modal/lead-form-modal.component';
import { CreateLeadDTO } from '../../dtos/CreateLeadDTO';
import { ProposalService } from '../../services/proposal.service';
import { ProposalFormModalComponent } from '../proposal-form-modal/proposal-form-modal.component';
import { ContractService } from '../../services/contract.service';
import { ContractFormModalComponent } from '../contract-form-modal/contract-form-modal.component';

L10n.load({
  "en-US": {
    kanban: { noCard: "Não há Leads" }
  }
});

@Component({
  selector: 'app-sales-funnel',
  templateUrl: './sales-funnel.component.html',
  styleUrls: ['./sales-funnel.component.scss']
})
export class SalesFunnelComponent implements OnInit {

  @ViewChild('kanban') kanbanObj!: KanbanComponent;

  cardSettings: CardSettingsModel = {
    contentField: 'personInfo',
    headerField: 'personName'
  };

  flagAwaken = true;
  slots: FunnelSlotDTO[] | undefined = [];
  leads: LeadsInFunnelDTO[] | undefined;

  constructor(
    private funnelSlotService: FunnelSlotService,
    private leadService: LeadService,
    private modalService: NgbModal,
    private proposalService: ProposalService,
    private contractService: ContractService
  ) { }

  ngOnInit(): void {
    this.loadSlots();
    this.loadLeads();
  }

  async loadSlots(){
    this.slots = await this.funnelSlotService.getSlots('SalesManagement');
  }

  async loadLeads(){
    this.leads = await this.leadService.getLeads();
  }

  awakenKanban() {
    const card : any = {
      id: 9999,
      funnelSlotId: '2',
      personName: ".",
      personInfo: "."
    }
    if(this.kanbanObj){
      this.kanbanObj.addCard(card)
      this.kanbanObj.deleteCard(card);
      this.flagAwaken = false;
    }
    return false;
  }

  dataSourceChanged(state: DataSourceChangedEventArgs): void {
    if(state.requestType === "cardChanged" && state?.changedRecords){
      this.cardTransition(state.changedRecords[0]);
    }
  }

  async cardTransition(lead: any){
    const currentSlot = parseInt(lead.funnelSlotId);

    if(currentSlot === 1 || currentSlot === 2){
      this.changestatusLead(lead);

    } else if (currentSlot === 3) {
      const proposalExists = await this.checksTheProposedExistence(lead);
      if(!proposalExists){
        this.openProposalModal(lead);
      } else {
        this.changestatusLead(lead);
      }

    } else if (currentSlot === 4) {
      const proposalExists = await this.checksTheProposedExistence(lead);
      if(proposalExists){
        const contractExists = await this.checksTheContractExistence(lead);
        if(!contractExists){
          this.openContractModal(lead, proposalExists);
        } else {
          this.changestatusLead(lead);
        }
      } else {
        this.kanbanObj.deleteCard(lead);
        lead.funnelSlotId = "2";
        this.kanbanObj.addCard(lead);
        this.changestatusLead(lead);
      }
    }
  }

  async changestatusLead(lead: any) {
    let leadTransform = this.leadsInFunnelToCreateLead(lead);

    await this.leadService.changeStatusLead(leadTransform);
  }

  async checksTheProposedExistence(lead: any) {
    const proposal = await this.proposalService.getProposalByLead(lead.id);
    if(proposal){
      return true;
    }
    return false;
  }

  async checksTheContractExistence(lead: any) {
    const contract = await this.contractService.getContractByLead(lead.id);
    if(contract){
      return true;
    }
    return false;
  }

  openProposalModal(lead: any){
    let leadTransform = this.leadsInFunnelToCreateLead(lead);

    const modalRef = this.modalService.open(ProposalFormModalComponent, { centered: true, size: 'lg'});
    modalRef.componentInstance.lead = leadTransform;
    modalRef.result.then(result => {
      if(result){
        this.changestatusLead(lead);
      } else {
        this.kanbanObj.deleteCard(lead);
        lead.funnelSlotId = "2";
        this.kanbanObj.addCard(lead);
        this.changestatusLead(lead);
      }
    });
  }

  openContractModal(lead: any, proposalExists: boolean) {
    let leadTransform = this.leadsInFunnelToCreateLead(lead);
    const modalRef = this.modalService.open(ContractFormModalComponent, { centered: true, size: 'lg'});
    modalRef.componentInstance.lead = leadTransform;
    modalRef.result.then(result => {
      if(result){
        this.changestatusLead(lead);
      } else if (proposalExists) {
        this.kanbanObj.deleteCard(lead);
        lead.funnelSlotId = "3";
        this.kanbanObj.addCard(lead);
        this.changestatusLead(lead);
      } else {
        this.kanbanObj.deleteCard(lead);
        lead.funnelSlotId = "2";
        this.kanbanObj.addCard(lead);
        this.changestatusLead(lead);
      }
    });
  }

  calculateResponsiveHeight(){
    const view = screen.height*0.6;
    return `${view}px`;
  }

  handleDoubleClickCard(event: any) {
    this.updateInitialLead(event.data);
  }

  leadsInFunnelToCreateLead(lead: LeadsInFunnelDTO): CreateLeadDTO  {
    let leadTransform = new CreateLeadDTO();
    leadTransform.id = lead.id;
    leadTransform.funnelSlotId = parseInt(lead.funnelSlotId);
    leadTransform.person.name = lead.personName;
    leadTransform.person.cpf = lead.personCpf;
    leadTransform.person.contact.email = lead.personEmail;
    leadTransform.person.contact.cellphone = lead.personCellphone;

    return leadTransform;
  }


  updateInitialLead(lead: LeadsInFunnelDTO) {
    let leadTransform = this.leadsInFunnelToCreateLead(lead);

    const modalRef = this.modalService.open(LeadFormModalComponent, { centered: true, size: 'lg'});
    modalRef.componentInstance.lead = leadTransform;
    modalRef.result.then(leadUpdated => {
      if(leadUpdated && leadUpdated.action === 'updated'){
        this.kanbanObj.deleteCard(lead);
        this.kanbanObj.addCard(new LeadsInFunnelDTO(leadUpdated.lead));
      } else if (leadUpdated && leadUpdated.action === 'delete'){
        this.kanbanObj.deleteCard(lead);
      }
    });
  }

  handleOpenModalNewLead(){
    const modalRef = this.modalService.open(LeadFormModalComponent, { centered: true, size: 'lg'});
    modalRef.result.then(lead => {
      if(lead){
        this.kanbanObj.addCard(new LeadsInFunnelDTO(lead));
      }
    });
  }

  dialogOpen(args: DialogEventArgs): void {
    args.cancel = true;
  }

}
