import { Component, OnInit } from '@angular/core';
import { ContractListDTO } from '../../dtos/ContractListDTO';
import { Contract } from '../../models/Contract';
import { ContractService } from '../../services/contract.service';

@Component({
  selector: 'app-contract-list',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.scss']
})
export class ContractListComponent implements OnInit {

  contracts: ContractListDTO[] | undefined;

  constructor(  
    private contractService: ContractService
  ) { }

  ngOnInit(): void {
    this.loadContracts();
  }

  async loadContracts() {
    const contract : any = await this.contractService.getContracts();
    this.contracts = contract[0];
  }

}
