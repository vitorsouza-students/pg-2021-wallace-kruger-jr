import { Component, OnInit } from '@angular/core';
import { ClientDTO } from '../../dtos/ClientDTO';
import { Client } from '../../models/Client';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {

  clients: ClientDTO[] | undefined;

  constructor(
    private clientService: ClientService
  ) { }

  ngOnInit(): void {
    this.loadClients();
  }

  async loadClients() {
    const client : any = await this.clientService.getClients();
    this.clients = client[0];
  }

}
