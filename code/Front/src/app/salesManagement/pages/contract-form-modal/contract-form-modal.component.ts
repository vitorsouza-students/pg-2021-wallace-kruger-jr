import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateLeadDTO } from '../../dtos/CreateLeadDTO';
import { ToastService } from 'src/app/shared/services/toats.service';
import { ContractService } from '../../services/contract.service';
import { Contract } from '../../models/Contract';
import { ProposalService } from '../../services/proposal.service';
import { Proposal } from '../../models/Proposal';
import ValidatesFormGroup from 'src/app/shared/utils/ValidatesFormGroup';
import { ContractStatusService } from '../../services/contractStatus.service';
import { ContractStatus } from '../../models/ContractStatus';
import { ContractDTO } from '../../dtos/ContractDTO';
import { ConfirmModalComponent } from 'src/app/shared/components/confirm-modal/confirm-modal.component';
import { jsPDF } from "jspdf";

@Component({
  selector: 'app-contract-form-modal',
  templateUrl: './contract-form-modal.component.html',
  styleUrls: ['./contract-form-modal.component.scss']
})
export class ContractFormModalComponent implements OnInit {

  proposalForm!: FormGroup;
  contractForm!: FormGroup;
  lead = new CreateLeadDTO();
  proposal: Proposal | undefined = new Proposal();
  contract: Contract | undefined = new Contract();
  contractDTO: ContractDTO | undefined = new ContractDTO();
  contractStatus: ContractStatus[] | undefined = [];
  isUpdate = false;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private proposalService: ProposalService,
    private contractService: ContractService,
    private contractStatusService: ContractStatusService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.buildProposalForm();
    this.buildContractForm();
    this.loadPrposal();
    this.loadcontractStatus();
    this.checkUpdated();
  }

  buildProposalForm(){
    this.proposalForm = this.formBuilder.group({
      description: [this.proposal?.description, Validators.required ],
      value: [this.proposal?.value, Validators.required ]
    });
  }

  async loadPrposal(){
    this.proposal = await this.proposalService.getProposalByLead(this.lead.id);
    this.buildProposalForm();
  }

  async loadcontractStatus(){
    this.contractStatus = await this.contractStatusService.getAll();
  }

  buildContractForm(){
    this.contractForm = this.formBuilder.group({
      initialDate: [this.contract?.initialDate, Validators.required ],
      finalDate: [this.contract?.finalDate, Validators.required ],
      status: [this.contract?.contractStatusId ? this.contract?.contractStatusId : 1, Validators.required ]
    }, {
      validators: [ValidatesFormGroup.validatorPeriod]
    });
  }

  async checkUpdated(){
    if(this.isUpdate){
      this.contract = await this.contractService.getContractByLead(this.lead.id);
      this.buildContractForm();
    }
  }

  isoDateToBrDate(dateString: any) {
    const date = new Date(dateString);
    const year = date.getFullYear();
    let month = date.getMonth()+1;
    let monthString = month.toString();
    let day = date.getDate();
    let dayString = day.toString();

    if (day < 10) {
      dayString = '0' + day;
    }

    if (month < 10) {
      monthString = '0' + month;
    }

    return `${dayString}/${monthString}/${year}`;
  }

  inputError(formName: FormGroup, propertyName: string, validatorName: string){
    return formName?.controls[propertyName]?.errors?.[validatorName] && formName?.controls[propertyName]?.['touched'];
  }

  handleSaveContract(){
    this.contractForm.markAllAsTouched();
    if (!this.contractForm.valid) {
      return;
    }

    this.getValuesForm();

    if (!this.isUpdate) {
      this.create()
    } else {
      this.update()
    }
  }

  getValuesForm() {
    const contractForm = this.contractForm.value;

    if(this.contractDTO){
      this.contractDTO.id = this.contract?.id;
      this.contractDTO.initialDate = contractForm.initialDate;
      this.contractDTO.finalDate = contractForm.finalDate;
      this.contractDTO.proposalId = this.proposal?.id;
      this.contractDTO.contractStatusId = contractForm.status;
    }
  }

  async create() {
    try {
      await this.contractService.createContract(this.contractDTO);
      this.toastService.show("Contrato criado com sucesso!", { classname: 'bg-success text-light' });
      this.activeModal.close(this.contract);
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  async update(){
    try {
      await this.contractService.updateContract(this.contractDTO);
      this.toastService.show("Contrato editado com sucesso!", { classname: 'bg-success text-light' });
      this.activeModal.close(this.contractDTO);
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  handleDeleteContract() {
    const modalRef = this.modalService.open(ConfirmModalComponent, { centered: true, size: "md"});
    modalRef.componentInstance.title = 'Deseja mesmo excluir o contrato?';
    modalRef.componentInstance.content = 'Confirme abaixo:';
    modalRef.componentInstance.negationText = 'Cancelar';
    modalRef.componentInstance.confirmationText = 'Excluir agora';

    modalRef.result.then(result => {
      if(result){
        this.delete();
      }
    });
  }

  async delete(){
    try {
      await this.contractService.deleteContract(this.contract ? this.contract.id : 0);
      this.toastService.show("Contrato excluído com sucesso!", { classname: 'bg-success text-light' });
      this.activeModal.close({ lead: this.lead, action: 'delete'});
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  handleDonwloadContract(){
    const doc = new jsPDF();
    const lineRange = 60;
    let line = 15;
    let linesProsal = (this.proposal?.description?.length) ? Math.floor(this.proposal?.description.length / lineRange) : 0 / lineRange;
    const charactersRemaining = (this.proposal?.description?.length) ? this.proposal?.description.length % lineRange : 0 % lineRange;

    doc.text('CONTRATO DE DESENVOLVIMENTO DE SOFTWARE', 30, line);
    line += 15;
    doc.text(`Data Inicial: ${this.isoDateToBrDate(this.contract?.initialDate)}`, 20, line);
    doc.text(`Data Final: ${this.isoDateToBrDate(this.contract?.finalDate)}`, 130, line);
    line += 15;

    let i = 1;
    for(i; i < linesProsal; i++){
      doc.text(`${this.proposal?.description?.substring((i-1)*lineRange,i*lineRange)}`, 20, line);
      line += 10;
    }
    i++;

    doc.text(`${this.proposal?.description?.substring((i-1)*lineRange,(i-1)*lineRange+charactersRemaining)}`, 20, line);
    line += 20;

    doc.text(`Valor: R$ ${this.proposal?.value?.toString().replace('.', ',')}`, 130, line);
    line += 15;

    doc.text(`____________________________`, 58, line);
    line += 5;
    doc.text(`Assinatura`, 88, line);

    doc.save(`Contrato ${this.lead.person.name}.pdf`);
  }

}
