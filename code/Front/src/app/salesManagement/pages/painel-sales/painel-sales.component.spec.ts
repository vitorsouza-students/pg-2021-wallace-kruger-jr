import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelSalesComponent } from './painel-sales.component';

describe('PainelSalesComponent', () => {
  let component: PainelSalesComponent;
  let fixture: ComponentFixture<PainelSalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PainelSalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
