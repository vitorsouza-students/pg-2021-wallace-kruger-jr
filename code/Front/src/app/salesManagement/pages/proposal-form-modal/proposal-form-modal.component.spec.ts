import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalFormModalComponent } from './proposal-form-modal.component';

describe('ProposalFormModalComponent', () => {
  let component: ProposalFormModalComponent;
  let fixture: ComponentFixture<ProposalFormModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProposalFormModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
