import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from 'src/app/shared/components/confirm-modal/confirm-modal.component';
import { ToastService } from 'src/app/shared/services/toats.service';
import { CreateLeadDTO } from '../../dtos/CreateLeadDTO';
import { Proposal } from '../../models/Proposal';
import { ProposalService } from '../../services/proposal.service';

@Component({
  selector: 'app-proposal-form-modal',
  templateUrl: './proposal-form-modal.component.html',
  styleUrls: ['./proposal-form-modal.component.scss']
})
export class ProposalFormModalComponent implements OnInit {

  proposalForm!: FormGroup;
  lead = new CreateLeadDTO();
  proposal: Proposal | undefined = new Proposal();
  isUpdate = false;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private proposalService: ProposalService,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.checkUpdated();
  }

  buildForm(){
    this.proposalForm = this.formBuilder.group({
      description: [this.proposal?.description, Validators.required ],
      value: [this.proposal?.value, Validators.required ]
    });
  }

  async checkUpdated(){
    if(this.isUpdate){
      this.proposal = await this.proposalService.getProposalByLead(this.lead.id);
      this.buildForm();
    }
  }


  inputError(formName: FormGroup, propertyName: string, validatorName: string){
    return formName?.controls[propertyName]?.errors?.[validatorName] && formName?.controls[propertyName]?.['touched'];
  }

  handleSaveProposal(){
    this.proposalForm.markAllAsTouched();
    if (!this.proposalForm.valid) {
      return;
    }

    this.getValuesForm();

    if (!this.isUpdate) {
      this.create()
    } else {
      this.update()
    }
  }

  getValuesForm() {
    const proposalForm = this.proposalForm.value;

    if(this.proposal) {
      this.proposal.description = proposalForm.description;
      this.proposal.sendDate = new Date();
      this.proposal.value = proposalForm.value;
      this.proposal.leadId = this.lead.id;
      this.proposal.lead.id = this.lead.id;
    }
  }

  async create(){
    try {
      await this.proposalService.createProposal(this.proposal);
      this.toastService.show("Proposta criada com sucesso!", { classname: 'bg-success text-light' });
      this.activeModal.close(this.proposal);
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  async update(){
    try {
      await this.proposalService.updateProposal(this.proposal);
      this.toastService.show("Proposta editada com sucesso!", { classname: 'bg-success text-light' });
      this.activeModal.close(this.proposal);
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  handleDeleteProposal() {
    const modalRef = this.modalService.open(ConfirmModalComponent, { centered: true, size: "md"});
    modalRef.componentInstance.title = 'Deseja mesmo excluir a proposta?';
    modalRef.componentInstance.content = 'Confirme abaixo:';
    modalRef.componentInstance.negationText = 'Cancelar';
    modalRef.componentInstance.confirmationText = 'Excluir agora';

    modalRef.result.then(result => {
      if(result){
        this.delete();
      }
    });
  }

  async delete(){
    try {
      await this.proposalService.deleteProposal(this.proposal ? this.proposal.id : 0);
      this.toastService.show("Proposta excluída com sucesso!", { classname: 'bg-success text-light' });
      this.activeModal.close({ lead: this.lead, action: 'delete'});
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

}
