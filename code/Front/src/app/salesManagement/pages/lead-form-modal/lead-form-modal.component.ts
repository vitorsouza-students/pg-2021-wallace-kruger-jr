import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from 'src/app/shared/components/confirm-modal/confirm-modal.component';
import { ToastService } from 'src/app/shared/services/toats.service';
import ValidatesFormGroup from 'src/app/shared/utils/ValidatesFormGroup';
import { CreateLeadDTO } from '../../dtos/CreateLeadDTO';
import { LeadService } from '../../services/lead.service';
import { ProposalFormModalComponent } from '../proposal-form-modal/proposal-form-modal.component';
import { ContractFormModalComponent } from '../contract-form-modal/contract-form-modal.component';
import { LeadToClientModalComponent } from '../lead-to-client-modal/lead-to-client-modal.component';

@Component({
  selector: 'app-lead-form-modal',
  templateUrl: './lead-form-modal.component.html',
  styleUrls: ['./lead-form-modal.component.scss']
})
export class LeadFormModalComponent implements OnInit {

  leadForm!: FormGroup;
  lead = new CreateLeadDTO();
  isUpdate = false;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private leadService: LeadService,
    private toastService: ToastService,
    private modalService: NgbModal,
    ) { }

  ngOnInit(): void {
    this.buildForm();
    this.checkUpdated();
  }

  buildForm(){
    this.leadForm = this.formBuilder.group({
      name: [this.lead?.person?.name, Validators.required ],
      cpf: [this.lead?.person?.cpf, Validators.required ],
      email: [this.lead?.person?.contact?.email, [Validators.required, Validators.email] ],
      cellphone: [this.lead?.person?.contact?.cellphone, Validators.required ],
    }, {
      validators: [ValidatesFormGroup.validatorCpf]
    });
  }

  checkUpdated() {
    if(this.lead.id){
      this.isUpdate = true;
    }
  }

  inputError(formName: FormGroup, propertyName: string, validatorName: string){
    return formName?.controls[propertyName]?.errors?.[validatorName] && formName?.controls[propertyName]?.['touched'];
  }

  handleSaveLead(){
    this.leadForm.markAllAsTouched();
    if (!this.leadForm.valid) {
      return;
    }

    this.getValuesForm();

    if (!this.isUpdate) {
      this.create()
    } else {
      this.update()
    }
  }

  getValuesForm() {
    const leadForm = this.leadForm.value;

    this.lead.funnelSlotId = (this.isUpdate) ? this.lead.funnelSlotId : 1;
    this.lead.person.name = leadForm.name;
    this.lead.person.cpf = leadForm.cpf;
    this.lead.person.contact.email = leadForm.email;
    this.lead.person.contact.cellphone = leadForm.cellphone;
    this.lead.person.address = null as any;
  }

  async create(){
    try {
      const lead = await this.leadService.createLead(this.lead);
      this.lead.id = lead?.id;
      this.toastService.show("Lead criado com sucesso!", { classname: 'bg-success text-light' });
      this.activeModal.close(this.lead);
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  async update(){
    try {
      await this.leadService.updateLead(this.lead);
      this.toastService.show("Lead atualizado com sucesso!", { classname: 'bg-success text-light' });
      this.activeModal.close({ lead: this.lead, action: 'updated'});
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  handleDeleteLead() {
    const modalRef = this.modalService.open(ConfirmModalComponent, { centered: true, size: "md"});
    modalRef.componentInstance.title = 'Deseja mesmo excluir a Lead?';
    modalRef.componentInstance.content = 'Confirme abaixo:';
    modalRef.componentInstance.negationText = 'Cancelar';
    modalRef.componentInstance.confirmationText = 'Excluir agora';

    modalRef.result.then(result => {
      if(result){
        this.delete();
      }
    });
  }

  async delete(){
    try {
      await this.leadService.deleteLead(this.lead.id);
      this.toastService.show("Lead excluída com sucesso!", { classname: 'bg-success text-light' });
      this.activeModal.close({ lead: this.lead, action: 'delete'});
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  handleEditProposal(){
    const modalRef = this.modalService.open(ProposalFormModalComponent, { centered: true, size: 'lg'});
    modalRef.componentInstance.lead = this.lead;
    modalRef.componentInstance.isUpdate = true;
    modalRef.result.then(result => {
      if(result && result.action === 'delete'){
        this.lead.funnelSlotId = 2;
        this.activeModal.close({ lead: this.lead, action: 'updated'});
      }
    });
  }

  handleEditContract() {
    const modalRef = this.modalService.open(ContractFormModalComponent, { centered: true, size: 'lg'});
    modalRef.componentInstance.lead = this.lead;
    modalRef.componentInstance.isUpdate = true;
    modalRef.result.then(result => {
      if(result && result.action === 'delete'){
        this.lead.funnelSlotId = 3;
        this.activeModal.close({ lead: this.lead, action: 'updated'});
      }
    });
  }

  handleDeclareWinLoss(gaint: boolean){
    const modalRef = this.modalService.open(ConfirmModalComponent, { centered: true, size: "md"});
    modalRef.componentInstance.title = `Deseja mesmo declarar ${gaint ? 'ganho' : 'perda'} para a Lead?`;
    modalRef.componentInstance.content = `Confirme abaixo:`;
    modalRef.componentInstance.negationText = 'Cancelar';
    modalRef.componentInstance.confirmationText = 'Declarar';

    modalRef.result.then(result => {
      if(result){
        this.declareWinLoss(gaint);
      }
    });
  }

  async declareWinLoss(gaint: boolean){
    if(!gaint){
      try {
        await this.leadService.declareWinLoss({id: this.lead.id, gaint: gaint});
        this.toastService.show("Foi declarado perda para a Lead!", { classname: 'bg-success text-light' });
        this.activeModal.close({ lead: this.lead, action: 'delete'});
      } catch (error : any) {
        this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
      }
    } else {
      const modalRef = this.modalService.open(LeadToClientModalComponent, { centered: true, size: "xl"});
      modalRef.componentInstance.lead = this.lead;
      modalRef.result.then(result => {
        if(result && result.action === 'create'){
          this.activeModal.close(undefined);
        }
      });
    }
  }

}
