import { Component, OnInit } from '@angular/core';
import { Proposal } from '../../models/Proposal';
import { ProposalService } from '../../services/proposal.service';

@Component({
  selector: 'app-proposal-list',
  templateUrl: './proposal-list.component.html',
  styleUrls: ['./proposal-list.component.scss']
})
export class ProposalListComponent implements OnInit {

  proposals: Proposal[] | undefined;

  constructor(  
    private proposalService: ProposalService
  ) { }

  ngOnInit(): void {
    this.loadProposals();
  }

  async loadProposals() {
    const proposals : any = await this.proposalService.getProposals();
    this.proposals = proposals[0];
    console.log(this.proposals);
    
  }

}
