import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadToClientModalComponent } from './lead-to-client-modal.component';

describe('LeadToClientModalComponent', () => {
  let component: LeadToClientModalComponent;
  let fixture: ComponentFixture<LeadToClientModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeadToClientModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadToClientModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
