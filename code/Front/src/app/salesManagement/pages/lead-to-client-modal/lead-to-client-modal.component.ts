import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { ToastService } from 'src/app/shared/services/toats.service';
import { ZipCodeService } from 'src/app/shared/services/zipCode.service';
import ValidatesFormGroup from 'src/app/shared/utils/ValidatesFormGroup';
import { CreateLeadDTO } from '../../dtos/CreateLeadDTO';
import { City } from '../../models/City';
import { Client } from '../../models/Client';
import { CityService } from '../../services/city.service';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-lead-to-client-modal',
  templateUrl: './lead-to-client-modal.component.html',
  styleUrls: ['./lead-to-client-modal.component.scss']
})
export class LeadToClientModalComponent implements OnInit {

  clientForm!: FormGroup;
  lead = new CreateLeadDTO();
  client : Client = new Client();
  ufs!: any;
  citys!: City[];

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private cityService: CityService,
    private zipCodeService: ZipCodeService,
    private toastService: ToastService,
    private profileService: ProfileService,
    private clientService: ClientService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.loadUfs();
    this.startClient();
  }

  buildForm(){
    this.clientForm = this.formBuilder.group({
      name: [this.lead.person.name, Validators.required ],
      cpf: [this.lead.person.cpf, Validators.required ],
      email: [this.lead.person.contact.email, [Validators.required, Validators.email] ],
      cellphone: [this.lead.person.contact.cellphone, Validators.required ],
      zipCode:['', Validators.required ],
      uf: [, Validators.required],
      city: [, Validators.required],
      street:['', Validators.required ],
      neighborhood:['', Validators.required ],
      numberAddress:['', Validators.required ],
      complement:['', Validators.required ],
      lat:['',],
      lng:['',],

    }, {
      validators: [ValidatesFormGroup.validatorCpf]
    });
  }

  inputError(formName: FormGroup, propertyName: string, validatorName: string){
    return formName?.controls[propertyName]?.errors?.[validatorName] && formName?.controls[propertyName]?.['touched'];
  }

  startClient() {
    const profile = this.profileService.getProfile();
    this.client.user.organizationId = profile.organizationId;
    this.client.person = this.lead.person
  }

  async loadUfs() {
    this.ufs = await this.cityService.getUfs();
    const defaultUf = this.ufs[0].uf;
    this.clientForm.controls['uf'].setValue(defaultUf);
    this.loadCitysByUf(defaultUf);
  }

  handleInsertZipCode() {
    const zipCode = this.clientForm.value.zipCode;
    if(zipCode.length >= 8) {
      this.buildAddressByZipCode(zipCode);     
    }    
  }

  async buildAddressByZipCode(zipCode: string) {
    try {
      const address : any = await this.zipCodeService.getAddressByZipCode(zipCode);
      this.clientForm.controls['uf'].setValue(address.uf);
      await this.loadCitysByUf(address.uf);
      const city = this.citys.find(city => city.name === address.localidade);
      this.clientForm.controls['city'].setValue(city?.id);
      this.clientForm.controls['neighborhood'].setValue(address.bairro);
      this.clientForm.controls['street'].setValue(address.logradouro);
      this.clientForm.controls['complement'].setValue(address.complemento);
      
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

  handleSelectedUf() {
    const uf = this.clientForm.value.uf;
    this.loadCitysByUf(uf);    
  }

  async loadCitysByUf(uf: string) {
    this.citys = await this.cityService.getCitys(uf) || [];
    const defaultCity = this.citys[1].id;
    this.clientForm.controls['city'].setValue(defaultCity);
  }

  handleSaveClient(){
    this.clientForm.markAllAsTouched();
    if (!this.clientForm.valid) {
      return;
    }

    this.getValuesForm();
    
    this.save();
  }

  getValuesForm() {
    const form = this.clientForm.value;

    this.client.person.name = form.name;
    this.client.person.cpf = form.cpf;
    this.client.person.contact.email = form.email;
    this.client.user.email = form.email;
    this.client.person.address.zipCode = form.zipCode;
    this.client.person.address.street = form.street;
    this.client.person.address.neighborhood = form.neighborhood;
    this.client.person.address.numberAddress = form.numberAddress;
    this.client.person.address.complement = form.complement;
    this.client.person.address.cityId = form.city;
    this.client.person.address.city.id = form.city;
  }

  async save(){
    try {
      await this.clientService.createLead(this.client);
      this.toastService.show("Cliente criado com sucesso!", { classname: 'bg-success text-light' });
      this.router.navigateByUrl("clientes");
      this.activeModal.close({ action: 'create'});
    } catch (error : any) {
      this.toastService.show(error.error.message, { classname: 'bg-danger text-light' });
    }
  }

}
