export class ContractDTO {
  id?: number;
  code?: string;
  initialDate?: Date | undefined;
  finalDate?: Date | undefined;
  proposalId?: number;
  contractStatusId?: number;
}
