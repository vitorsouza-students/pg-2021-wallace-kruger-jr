import { Person } from "../models/Person";

export class CreateLeadDTO{
  id?: number;
  funnelSlotId!: number;
  person: Person;

  constructor(){
    this.person = new Person();
  }
}
