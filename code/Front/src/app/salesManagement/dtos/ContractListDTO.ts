export class ContractListDTO {
    id?: number;
    code?: string;
    initialDate?: string;
    finalDate?: string;
    clientPersonName?: string;
    contractStatusName?: string;
}