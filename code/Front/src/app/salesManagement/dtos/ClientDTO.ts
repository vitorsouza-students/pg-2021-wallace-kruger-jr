export class ClientDTO {
    id?: number;
    code?: string;
    personName?: string;
    personCpf?: string;
    personContactEmail?: string;
    personContactCellphone?: string;
    addressNeighborhood?: string;
    addressCityName?: string;
    addressCityUf?: string;
}