import { FunnelSlot } from "./FunnelSlot";
import { Person } from "./Person";

export class Lead {
  id?: number;
  gaint!: boolean;
  funnelSlotId!: number;
  funnelSlot: FunnelSlot;
  personId!: number;
  person: Person;

  constructor(){
    this.funnelSlot = new FunnelSlot();
    this.person = new Person();
  }
}
