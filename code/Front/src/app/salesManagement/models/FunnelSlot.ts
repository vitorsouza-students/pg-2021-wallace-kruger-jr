export class FunnelSlot {
  id!: number;
  title!: string;
  color!: string;
  orderFunnel!: string;
}
