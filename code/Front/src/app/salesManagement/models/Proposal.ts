import { Lead } from "./Lead";

export class Proposal {
  id?: number;
  code?: string;
  description?: string;
  sendDate?: Date;
  value?: number;
  leadId?: number;
  lead: Lead;

  constructor(){
    this.lead = new Lead();
  }
}
