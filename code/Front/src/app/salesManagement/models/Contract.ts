import { Client } from "./Client";
import { ContractStatus } from "./ContractStatus";
import { Proposal } from "./Proposal";

export class Contract {
  id?: number;
  code?: string;
  initialDate?: Date | undefined;
  finalDate?: Date | undefined;
  proposalId?: number;
  proposal?: Proposal;
  contractStatusId?: number;
  contractStatus?: ContractStatus;
  clientId?: number;
  client?: Client | null;

  constructor(){
    this.proposal = new Proposal();
    this.contractStatus = new ContractStatus;
    this.client = new Client();
  }
}
