import { City } from "./City";

export class Address {
  id!: number;
  zipCode!: string;
  street!: string;
  neighborhood!: string;
  numberAddress!: string;
  complement!: string;
  lat!: number;
  lng!: number;
  cityId!: number;
  city!: City;

  constructor(){
    this.city = new City();
  }
}
