export class Contact {
  id!: number;
  email!: string;
  telephone!: string;
  cellphone!: string;
}
