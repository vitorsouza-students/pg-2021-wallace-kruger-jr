import { Contact } from 'src/app/userManagement/models/Contact';
import { Address } from './Address';
export class Person {
  id!: number;
  name!: string;
  cpf!: string;
  addressId!: number;
  address: Address;
  contactId!: number;
  contact: Contact;

  constructor() {
    this.address = new Address();
    this.contact = new Contact();
  }
}
