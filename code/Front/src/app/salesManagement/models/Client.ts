import User from "src/app/userManagement/models/User";
import { Contract } from "./Contract";
import { Lead } from "./Lead";
import { Person } from "./Person";

export class Client {
  id!: number;
  code!: string;
  userId!: number;
  user: User;
  personId!: number;
  person: Person;
  leads: Lead[];
  contracts: Contract[];

  constructor() {
    this.user = new User();
    this.person = new Person();
    this.leads = [];
    this.contracts = [];
  }
}
