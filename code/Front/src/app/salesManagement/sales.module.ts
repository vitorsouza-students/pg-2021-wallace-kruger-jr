import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgxMaskModule } from 'ngx-mask';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { KanbanModule } from '@syncfusion/ej2-angular-kanban';
import { SalesFunnelComponent } from './pages/sales-funnel/sales-funnel.component';
import { ClientListComponent } from './pages/client-list/client-list.component';
import { ProposalListComponent } from './pages/proposal-list/proposal-list.component';
import { ContractListComponent } from './pages/contract-list/contract-list.component';
import { PainelSalesComponent } from './pages/painel-sales/painel-sales.component';
import { LeadFormModalComponent } from './pages/lead-form-modal/lead-form-modal.component';
import { ProposalFormModalComponent } from './pages/proposal-form-modal/proposal-form-modal.component';
import { ContractFormModalComponent } from './pages/contract-form-modal/contract-form-modal.component';
import { LeadToClientModalComponent } from './pages/lead-to-client-modal/lead-to-client-modal.component';


@NgModule({
  declarations: [
    SalesFunnelComponent,
    ClientListComponent,
    ProposalListComponent,
    ContractListComponent,
    PainelSalesComponent,
    LeadFormModalComponent,
    ProposalFormModalComponent,
    ContractFormModalComponent,
    LeadToClientModalComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
    NgbModule,
    SharedModule,
    KanbanModule
  ]
})
export class SalesModule { }
