import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

  constructor(
    private router: Router,
    private cookieService: CookieService
  ) {

  }
  async canActivate(): Promise<boolean> {
    if (this.cookieService.get('token')) {
      return true;
    }

    this.router.navigateByUrl("login");
    return false;
  }


}
