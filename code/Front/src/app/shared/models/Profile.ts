export class Profile {
  userId?: number;
  email?: string;
  client?: boolean;
  collaborator?: boolean;
  manager?: boolean;
  organizationId?: number;
  tenant?: string;
  checked?: boolean;
}
