import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.scss']
})
export class PainelComponent implements OnInit {

  constructor(
    private router: Router,
    private cookieService: CookieService
    ) { }

  ngOnInit(): void {
  }

  handleLogout(){
    this.cookieService.deleteAll();
    this.router.navigateByUrl("login");
  }

}
