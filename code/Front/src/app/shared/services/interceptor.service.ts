import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse }
from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Observable, throwError } from 'rxjs';
import { finalize, catchError } from 'rxjs/operators';
import { LoadingService } from './loading.service';
import { ProfileService } from './profile.service';

@Injectable()

export class Interceptor implements HttpInterceptor {

  constructor(
    private cookieService: CookieService,
    private profileService: ProfileService,
    private loadingService: LoadingService
    ) { }

  intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

    if (!request.params.get('disableLoading')) {
      this.loadingService.showLoading(true);
    }

    if(this.cookieService.get('token')){
      request = request.clone(this.setHeaders());
    }


    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        this.loadingService.showLoading(false);
          return throwError(error);
      }),
      finalize(() => this.loadingService.showLoading(false))
    );
  }


  setHeaders() {
    const token = this.cookieService.get('token');
    const tenant = this.profileService.getProfile().tenant ? <string>this.profileService.getProfile().tenant : '';

    return {
        setHeaders: {
            'token': token,
            'tenant': tenant,
        }
    }
  }


}
