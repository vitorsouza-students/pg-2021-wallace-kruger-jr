import { Injectable } from '@angular/core';
import jwt_decode from "jwt-decode";
import { CookieService } from 'ngx-cookie-service';
import { Profile } from '../models/Profile';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private cookieService: CookieService
   ) { }

  public getProfile(): Profile{
    return jwt_decode(this.cookieService.get('token'));
  }
}
