import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ZipCodeService {

  private urlBase = `${environment.URL_GB_BACK}/zipcodes`;

  constructor(
    private http: HttpClient,
  ) { }

  public getAddressByZipCode(zipCode: string) {
    return this.http.get(`${this.urlBase}/${zipCode}`).toPromise();
  }
}
